﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using HardwareInspection.Model;
using HardwareInspection.Repository;
using Microsoft.Reporting.WinForms;

namespace HardwareInspection
{
    public partial class FrmReportViewEquipment : Form
    {
        static readonly Context DbContext = new Context();
        readonly EquipmentRepository _equRepo = new EquipmentRepository(DbContext);
        readonly EmployeeRepository _empRepo = new EmployeeRepository(DbContext);
        ReportDataSource _rep1;

        public FrmReportViewEquipment()
        {
            InitializeComponent();
        }

        private void FrmReportViewEquipment_Load(object sender, EventArgs e)
        {
            DataTable table = GetTable();
            _rep1 = new ReportDataSource("DataSet1", table);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(_rep1);
            reportViewer1.RefreshReport();
        }
        public DataTable GetTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Номер", typeof(string));
            table.Columns.Add("Наименование", typeof(string));
            table.Columns.Add("Инвентарный_номер", typeof(string));
            table.Columns.Add("Сотрудник", typeof(string));
            int i = 0;
            foreach (var list in _equRepo.Equipments.Where(n => n.Condition == Equipment.ConditionOfEquipment.Bad))
            {
                Employee emp = _empRepo.Employees.Find(x => x.Id == list.EmployeeId);
                table.Rows.Add(++i, list.NameEquipment, list.InventoryNumber, emp.LastName);
            }
            return table;
        }
    }
}
