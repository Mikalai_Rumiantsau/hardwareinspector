﻿using HardwareInspection.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using HardwareInspection.Repository;
using HardwareInspection.Services;

namespace HardwareInspection
{
    public partial class frmMain : Form
    {
        static readonly Context DbContext = new Context();

        readonly EmployeeRepository _empRepo = new EmployeeRepository(DbContext);
        readonly EquipmentRepository _equRepo = new EquipmentRepository(DbContext);
        readonly MaterialRepository _matRepo = new MaterialRepository(DbContext);
        readonly PartRepository _partRepo = new PartRepository(DbContext);
        readonly HardwareRepairRepository _hardRepo = new HardwareRepairRepository(DbContext);
        readonly MaterialRepairRepository _matRepairdRepo = new MaterialRepairRepository(DbContext);
        readonly PartRepairRepository _partRepairdRepo = new PartRepairRepository(DbContext);
        readonly OrganizationRepository _orgRepairdRepo = new OrganizationRepository(DbContext);
        readonly AccountingRepairRepository _accountingRepairRepo = new AccountingRepairRepository(DbContext);

        public EmployeeServices EmpServices = new EmployeeServices();
        public EquipmentServices EquServices = new EquipmentServices();
        public MaterialServices MatServices = new MaterialServices();
        public PartServices PartServices = new PartServices();
        public OrganizationServices OrgServices = new OrganizationServices();
       
        private Employee _tmpEmployee;
        private Employee _cmbEmployee;
        private Equipment _tmpEquipment;
        private Material _tmpMaterial;
        private Organization _tmpOrganization;
        private Part _tmpPart;

        private HardwareRepair _tmpHardwareRepair;

        private readonly List<int> _materialsIdList = new List<int>();
        private readonly List<int> _partsIdList = new List<int>();

        private string _searchCmbBoxEmployerr;
        public frmMain()
        {
            InitializeComponent();
        }

        #region FirstInitDatabase (remove!!!)
        private void button1_Click(object sender, EventArgs e)
        {
            using (var context = new Context())
            {
                try
                {
                    context.Employees.Add(new Employee() { FirstName = "FirstName", LastName = "LastName", Patronymic = "Patronymic", EmployeeNumber = "0000" });
                    context.Employees.Add(new Employee() { FirstName = "FirstName_1", LastName = "LastName_1", Patronymic = "Patronymic_1", EmployeeNumber = "1111" });
                    context.Employees.Add(new Employee() { FirstName = "FirstName_2", LastName = "LastName_2", Patronymic = "Patronymic_2", EmployeeNumber = "2222" });
                    context.Employees.Add(new Employee() { FirstName = "FirstName_3", LastName = "LastName_3", Patronymic = "Patronymic_3", EmployeeNumber = "3333" });

                    context.Equipments.Add(new Equipment() { EmployeeId = 1, NameEquipment = "Equipment_1", InventoryNumber = "12000", Condition = Equipment.ConditionOfEquipment.Good });
                    context.Equipments.Add(new Equipment() { EmployeeId = 1, NameEquipment = "Equipment_2", InventoryNumber = "12001", Condition = Equipment.ConditionOfEquipment.Good });
                    context.Equipments.Add(new Equipment() { EmployeeId = 1, NameEquipment = "Equipment_3", InventoryNumber = "12002", Condition = Equipment.ConditionOfEquipment.Bad });
                    context.Equipments.Add(new Equipment() { EmployeeId = 2, NameEquipment = "Equipment_4", InventoryNumber = "12003", Condition = Equipment.ConditionOfEquipment.Bad });
                    context.Equipments.Add(new Equipment() { EmployeeId = 2, NameEquipment = "Equipment_5", InventoryNumber = "12004", Condition = Equipment.ConditionOfEquipment.Good });
                    context.Equipments.Add(new Equipment() { EmployeeId = 2, NameEquipment = "Equipment_6", InventoryNumber = "12005", Condition = Equipment.ConditionOfEquipment.Good });
                    context.Equipments.Add(new Equipment() { EmployeeId = 3, NameEquipment = "Equipment_7", InventoryNumber = "12006", Condition = Equipment.ConditionOfEquipment.Bad });

                    context.Materials.Add(new Material() { EmployeeId = 1, NameMaterial = "Material_1", UnitOfMeasure = "шт.", Quantity = 10 });
                    context.Materials.Add(new Material() { EmployeeId = 1, NameMaterial = "Material_2", UnitOfMeasure = "шт.", Quantity = 11 });
                    context.Materials.Add(new Material() { EmployeeId = 1, NameMaterial = "Material_3", UnitOfMeasure = "шт.", Quantity = 12 });
                    context.Materials.Add(new Material() { EmployeeId = 2, NameMaterial = "Material_4", UnitOfMeasure = "шт.", Quantity = 13 });
                    context.Materials.Add(new Material() { EmployeeId = 2, NameMaterial = "Material_5", UnitOfMeasure = "шт.", Quantity = 14 });
                    context.Materials.Add(new Material() { EmployeeId = 2, NameMaterial = "Material_6", UnitOfMeasure = "шт.", Quantity = 15 });
                    context.Materials.Add(new Material() { EmployeeId = 3, NameMaterial = "Material_7", UnitOfMeasure = "шт.", Quantity = 16 });
                    context.Materials.Add(new Material() { EmployeeId = 3, NameMaterial = "Material_8", UnitOfMeasure = "шт.", Quantity = 17 });
                    context.Materials.Add(new Material() { EmployeeId = 3, NameMaterial = "Material_9", UnitOfMeasure = "шт.", Quantity = 18 });
                    context.Materials.Add(new Material() { EmployeeId = 3, NameMaterial = "Material_10", UnitOfMeasure = "шт.", Quantity = 19 });
                    context.Materials.Add(new Material() { EmployeeId = 3, NameMaterial = "Material_11", UnitOfMeasure = "шт.", Quantity = 20 });

                    context.Parts.Add(new Part() { EmployeeId = 1, NamePart = "Part_1", PartNumber = "Part_Number_1", UnitOfMeasure = "шт.", Quantity = 1 });
                    context.Parts.Add(new Part() { EmployeeId = 1, NamePart = "Part_2", PartNumber = "Part_Number_2", UnitOfMeasure = "шт.", Quantity = 1 });
                    context.Parts.Add(new Part() { EmployeeId = 2, NamePart = "Part_3", PartNumber = "Part_Number_3", UnitOfMeasure = "шт.", Quantity = 1 });
                    context.Parts.Add(new Part() { EmployeeId = 2, NamePart = "Part_4", PartNumber = "Part_Number_4", UnitOfMeasure = "шт.", Quantity = 1 });
                    context.Parts.Add(new Part() { EmployeeId = 3, NamePart = "Part_5", PartNumber = "Part_Number_5", UnitOfMeasure = "шт.", Quantity = 1 });
                    context.Parts.Add(new Part() { EmployeeId = 3, NamePart = "Part_6", PartNumber = "Part_Number_6", UnitOfMeasure = "шт.", Quantity = 1 });

                    context.Organizations.Add(new Organization() { NameOrganization = "Organization_1", Address = "Adress_1", Phone = "Phone_1"});
                    context.Organizations.Add(new Organization() { NameOrganization = "Organization_2", Address = "Adress_2", Phone = "Phone_2" });
                    context.Organizations.Add(new Organization() { NameOrganization = "Organization_3", Address = "Adress_3", Phone = "Phone_3" });
                    context.Organizations.Add(new Organization() { NameOrganization = "Organization_4", Address = "Adress_4", Phone = "Phone_4" });
                    context.Organizations.Add(new Organization() { NameOrganization = "Organization_5", Address = "Adress_5", Phone = "Phone_5" });

                    context.SaveChanges();
                }
                catch
                {
                    MessageBox.Show("Ошибка в процесси создания и инициализации БД", "Ошибка!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        #endregion


        private void FrmMain_Load(object sender, EventArgs e)
        {
            RefreshEmployeesGrid();
            tabControl1.SelectedIndex = 0;
            button1.Visible = false;
            lblIdMaterial.Visible = false;
            lblIdEmployee.Visible = false;
            lblIdEquipment.Visible = false;
            lblIdEquipmentRepair.Visible = false;
            lblIdEquipmentRepairAccount.Visible = false;
            lblCountMaterialForRepair.Visible = false;
            lblIdOrganizationForRepair.Visible = false;
            lblIdMaterialRepair.Visible = false;
            lblIdPart.Visible = false;
            lblIdPartRepair.Visible = false;
            lblCountMaterialForRepair.Visible = false;
            lblCountPartForRepair.Visible = false;
        }

        #region TabPage
        private void tabPage1_Enter(object sender, EventArgs e)
        {
            RefreshEmployeesGrid();
        }
        private void tabPage2_Enter(object sender, EventArgs e)
        {
            RefreshEquipmentsGrid();
        }

        private void tabPage3_Enter(object sender, EventArgs e)
        {
            RefreshMaterialsGrid(grdMaterials, 0);
        }

        private void tabPage4_Enter(object sender, EventArgs e)
        {
            RefreshPartsGrid(grdParts,0);
        }

        private void repairOnItsOwn_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage5;
          //  tabPage1.Text = "Работа с базой сотрудников";
        }
        #endregion

        #region MenuItems

        private void просмотрпечатьИсторииРемонтовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmWorkWithRepair frmWorkWithRepair = new FrmWorkWithRepair();
            frmWorkWithRepair.ShowDialog();
        }
        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAbout frmAbout = new FrmAbout();
            frmAbout.ShowDialog();
        }
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void сотрудникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage1;
            tabPage1.Text = "Работа с базой сотрудников";
        }

        private void оборудованиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
            tabPage2.Text = "Работа с базой оборудования";
        }

        private void материалыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage3;
            tabPage3.Text = "Работа с базой материалов";
        }
        private void запасныеЧастиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage4;
            tabPage4.Text = "Работа с базой запасных частей";
        }
        private void собственнымиСиламиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage5;
            tabPage5.Text = "Ремонт собственными силами";
        }

        private void стороннимиОрганизациямиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage6;
            tabPage6.Text = "Ремонты сторонними организациями";
        }
        private void ремонтныеОрганизацииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage6;
            tabPage6.Text = "Ремонты сторонними организациями";
        }

        #endregion

        #region OrganizationGrid
        private void RefreshOrganizationGrid()
        {
            OrgServices.ClearOrganizationsGrid(grdOrganizationForRepair);
            
            try
            {
                foreach (var list in _orgRepairdRepo.Organizations)
                {
                    OrgServices.AddToGrid(grdOrganizationForRepair,list);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void grdOrganizationForRepair_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewItem parent;
            if (grdOrganizationForRepair.SelectedItems.Count > 0)
            {
                parent = grdOrganizationForRepair.SelectedItems[0];
                lblIdOrganizationForRepair.Text = grdOrganizationForRepair.SelectedItems[0].Text;
                txtNameOrganizationForRepair.Text = parent.SubItems[1].Text;
                txtAdresOrganizationForRepair.Text = parent.SubItems[2].Text;
                txtPhoneOrganizationForRepair.Text = parent.SubItems[3].Text;
                //txtTabNom.Text = parent.SubItems[4].Text;
                _tmpOrganization = _orgRepairdRepo.Organizations.Find(x => x.Id == Convert.ToInt32(lblIdOrganizationForRepair.Text));
            }
        }


        #endregion

        #region EmployeesGrid

        private void RefreshEmployeesGrid()
        {
            EmpServices.ClearEmployeesGrid(grdEmployees);
            try
            {
                foreach (var list in _empRepo.Employees)
                {
                    EmpServices.AddToGrid(grdEmployees, list);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void grdEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewItem parent;
            if (grdEmployees.SelectedItems.Count > 0)
            {
                parent = grdEmployees.SelectedItems[0];
                lblIdEmployee.Text = grdEmployees.SelectedItems[0].Text;
                txtName.Text = parent.SubItems[1].Text;
                txtSoname.Text = parent.SubItems[2].Text;
                txtPatronymic.Text = parent.SubItems[3].Text;
                txtTabNom.Text = parent.SubItems[4].Text;
                _tmpEmployee = _empRepo.Employees.Find(x => x.Id == Convert.ToInt32(lblIdEmployee.Text));
            }
        }

        #endregion

        #region EquipmentsGrid

        private void RefreshEquipmentsGrid()
        {
            EquServices.ClearEquipmentsGrid(grdEquipments);
            cmbBoxEmployeersForEquipments.Items.Clear();
            try
            {
                foreach (var list in _equRepo.Equipments)
                {
                    EquServices.AddToGrid(grdEquipments, list);
                }
                foreach (var list in _empRepo.Employees)
                {
                    _searchCmbBoxEmployerr = list.FirstName + " " + list.LastName +" " + list.Patronymic + " " + list.EmployeeNumber;
                    cmbBoxEmployeersForEquipments.Items.Add(list.FirstName + " " + list.LastName + " " + list.Patronymic + " " + list.EmployeeNumber);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void grdEquipments_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ListViewItem parent;
            if (grdEquipments.SelectedItems.Count > 0)
            {
                parent = grdEquipments.SelectedItems[0];
                lblIdEquipment.Text = grdEquipments.SelectedItems[0].Text;
                txtNameEquipment.Text = parent.SubItems[1].Text;
                txtInventoryNumberEquipment.Text = parent.SubItems[2].Text;
                cBoxCondition.SelectedIndex = cBoxCondition.FindString(parent.SubItems[3].Text);

                _tmpEquipment = _equRepo.Equipments.Find(x => x.Id == Convert.ToInt32(lblIdEquipment.Text));
                _cmbEmployee = _empRepo.Employees.Find( x => x.Id == _tmpEquipment.EmployeeId );
                _searchCmbBoxEmployerr = _cmbEmployee.FirstName + " " + _cmbEmployee.LastName + " " + _cmbEmployee.Patronymic + " " + _cmbEmployee.EmployeeNumber;
                cmbBoxEmployeersForEquipments.SelectedIndex = cmbBoxEmployeersForEquipments.FindString(_searchCmbBoxEmployerr);
            }
        }


        #endregion

        #region MaterialsGrid
        private void RefreshMaterialsGrid(ListView grdView, int where)
        {
            MatServices.ClearMaterialsGrid(grdView, where);
            cmbBoxEmployeersForMaterials.Items.Clear();
            try
            {
                foreach (var list in _matRepo.Materials)
                {
                    MatServices.AddToGrid(grdView, list);
                }
                foreach (var list in _empRepo.Employees)
                {
                    _searchCmbBoxEmployerr = list.FirstName + " " + list.LastName + " " + list.Patronymic + " " + list.EmployeeNumber;
                    cmbBoxEmployeersForMaterials.Items.Add(list.FirstName + " " + list.LastName + " " + list.Patronymic + " " + list.EmployeeNumber);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void grdMaterials_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewItem parent;
            if (grdMaterials.SelectedItems.Count > 0)
            {
                parent = grdMaterials.SelectedItems[0];
                lblIdMaterial.Text = grdMaterials.SelectedItems[0].Text;
                txtNameMaterial.Text = parent.SubItems[1].Text;
                txtEdIzmMaterial.Text = parent.SubItems[2].Text;
                txtQuantityMaterial.Text = parent.SubItems[3].Text;

                _tmpMaterial = _matRepo.Materials.Find(x => x.Id == Convert.ToInt32(lblIdMaterial.Text));
                _cmbEmployee = _empRepo.Employees.Find(x => x.Id == _tmpMaterial.EmployeeId);
                _searchCmbBoxEmployerr = _cmbEmployee.FirstName + " " + _cmbEmployee.LastName + " " + _cmbEmployee.Patronymic + " " + _cmbEmployee.EmployeeNumber;
                cmbBoxEmployeersForMaterials.SelectedIndex = cmbBoxEmployeersForMaterials.FindString(_searchCmbBoxEmployerr);
            }
        }
        #endregion

        #region PartsGrid

        private void RefreshAccountingRepairGrid(ListView grdView, int where)
        {
            PartServices.ClearPartsGrid(grdView, where);
            cmbBoxEmployeersForParts.Items.Clear();
            try
            {
                foreach (var list in _partRepo.Parts)
                {
                    PartServices.AddToGrid(grdView, list);
                }
                foreach (var list in _empRepo.Employees)
                {
                    _searchCmbBoxEmployerr = list.FirstName + " " + list.LastName + " " + list.Patronymic + " " + list.EmployeeNumber;
                    cmbBoxEmployeersForParts.Items.Add(list.FirstName + " " + list.LastName + " " + list.Patronymic + " " + list.EmployeeNumber);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void RefreshPartsGrid(ListView grdView,int where)
        {
            PartServices.ClearPartsGrid(grdView, where);
            cmbBoxEmployeersForParts.Items.Clear();
            try
            {
                foreach (var list in _partRepo.Parts)
                {
                    PartServices.AddToGrid(grdView, list);
                }
                foreach (var list in _empRepo.Employees)
                {
                    _searchCmbBoxEmployerr = list.FirstName + " " + list.LastName + " " + list.Patronymic + " " + list.EmployeeNumber;
                    cmbBoxEmployeersForParts.Items.Add(list.FirstName + " " + list.LastName + " " + list.Patronymic + " " + list.EmployeeNumber);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void grdParts_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewItem parent;
            if (grdParts.SelectedItems.Count > 0)
            {
                parent = grdParts.SelectedItems[0];
                lblIdPart.Text = grdParts.SelectedItems[0].Text;
                txtNamePart.Text = parent.SubItems[1].Text;
                txtPartNumber.Text = parent.SubItems[2].Text;
                txtEdIzmPart.Text = parent.SubItems[3].Text;
                txtQuantityPart.Text = parent.SubItems[4].Text;
                _tmpPart = _partRepo.Parts.Find(x => x.Id == Convert.ToInt32(lblIdPart.Text));
                _cmbEmployee = _empRepo.Employees.Find(x => x.Id == _tmpPart.EmployeeId);
                _searchCmbBoxEmployerr = _cmbEmployee.FirstName + " " + _cmbEmployee.LastName + " " + _cmbEmployee.Patronymic + " " + _cmbEmployee.EmployeeNumber;
                cmbBoxEmployeersForParts.SelectedIndex = cmbBoxEmployeersForParts.FindString(_searchCmbBoxEmployerr);
            }
        }



        #endregion

        #region OrganizationAction
        private void txtFilterOrganizationForRepair_TextChanged(object sender, EventArgs e)
        {
            OrgServices.ClearOrganizationsGrid(grdOrganizationForRepair);
            foreach (var list in _orgRepairdRepo.Organizations.Where(n => n.NameOrganization.Contains(txtFilterOrganizationForRepair.Text)))
            {
                OrgServices.AddToGrid(grdOrganizationForRepair, list);
            }
            if (grdOrganizationForRepair.Items.Count == 0)
            {
                OrgServices.AddToGrid(grdOrganizationForRepair, new Organization() {Id = 0, NameOrganization = "Нет такого", Address = "Нет такого", Phone = "Нет такого" });
            }
        }

        private void updateOrganization_Click(object sender, EventArgs e)
        {
            int k = 0;
            if (_tmpOrganization.NameOrganization != txtNameOrganizationForRepair.Text)
            {
                _tmpOrganization.NameOrganization = txtNameOrganizationForRepair.Text;
                k++;
            }
            if (_tmpOrganization.Address != txtAdresOrganizationForRepair.Text)
            {
                _tmpOrganization.Address = txtAdresOrganizationForRepair.Text;
                k++;
            }
            if (_tmpOrganization.Phone != txtPhoneOrganizationForRepair.Text)
            {
                _tmpOrganization.Phone = txtPhoneOrganizationForRepair.Text;
                k++;
            }

            if (k != 0)
            {
                _orgRepairdRepo.Update(_tmpOrganization);
                RefreshOrganizationGrid();
            }
        }

        private void addOrganization_Click(object sender, EventArgs e)
        {
            int k = 0;
            if (_tmpOrganization.NameOrganization != txtNameOrganizationForRepair.Text)
            {
                _tmpOrganization.NameOrganization = txtNameOrganizationForRepair.Text;
                k++;
            }
            if (_tmpOrganization.Address != txtAdresOrganizationForRepair.Text)
            {
                _tmpOrganization.Address = txtAdresOrganizationForRepair.Text;
                k++;
            }
            if (_tmpOrganization.Phone != txtPhoneOrganizationForRepair.Text)
            {
                _tmpOrganization.Phone = txtPhoneOrganizationForRepair.Text;
                k++;
            }
            if (k > 2)
            {
                _orgRepairdRepo.Add(_tmpOrganization);
                RefreshOrganizationGrid();
            }
            else
            {
                MessageBox.Show("Проверьте исходные данные!!!", "Ошибка в исходных данных", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        private void deleteOrganization_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно желаете удалить запись?", "Удаление записи", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _orgRepairdRepo.Delete(_tmpOrganization);
                RefreshOrganizationGrid();
            }
        }

        #endregion


        #region EmployeesAction
        private void filterEmployee_TextChanged(object sender, EventArgs e)
        {
            EmpServices.ClearEmployeesGrid(grdEmployees);
            foreach (var list in _empRepo.Employees.Where(n => n.LastName.Contains(filterEmployee.Text)))
            {
                EmpServices.AddToGrid(grdEmployees, list);
            }
            if (grdEmployees.Items.Count == 0)
            {
                EmpServices.AddToGrid(grdEmployees, new Employee() { Id = 0, FirstName = "Нет такого", LastName = "Нет такого", Patronymic = "Нет такого", EmployeeNumber = "Нет такого" });
            }
        }

        private void updateEmployee_Click(object sender, EventArgs e)
        {
            int k = 0, s = 0;
            if (_tmpEmployee.FirstName != txtName.Text)
            {
                _tmpEmployee.FirstName = txtName.Text;
                k++;
            }
            if (_tmpEmployee.LastName != txtSoname.Text)
            {
                _tmpEmployee.LastName = txtSoname.Text;
                k++;
                s=1;
            }
            if (_tmpEmployee.Patronymic != txtPatronymic.Text)
            {
                _tmpEmployee.Patronymic = txtPatronymic.Text;
                k++;
            }
            if (_tmpEmployee.EmployeeNumber != txtTabNom.Text)
            {
                _tmpEmployee.EmployeeNumber = txtTabNom.Text;
                k++;
            }
            if (k != 0 || s == 1)
            {
                _empRepo.Update(_tmpEmployee);
                RefreshEmployeesGrid();
            }
        }

        private void addEmployee_Click(object sender, EventArgs e)
        {
            int k = 0;
            if (txtName.Text != "")
            {
                k++;
            }
            if (txtSoname.Text != "")
            {
                k++;
            }
            if (txtPatronymic.Text != "")
            {
                k++;
            }
            if (txtTabNom.Text != "")
            {
                k++;
            }
            if (k == 4)
            {
                _empRepo.Add(new Employee() {FirstName = txtName.Text, LastName = txtSoname.Text, Patronymic = txtPatronymic.Text, EmployeeNumber = txtTabNom.Text});
                RefreshEmployeesGrid();
            }
            else
            {
                MessageBox.Show("Проверьте исходные данные!!!", "Ошибка в исходных данных", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void deleteEmployee_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно желаете удалить запись?", "Удаление записи", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _empRepo.Delete(_tmpEmployee);
                RefreshEmployeesGrid();
            }
        }



        #endregion

        #region EquipmentAction

        private void txtFilterEquipments_TextChanged(object sender, EventArgs e)
        {
            EquServices.ClearEquipmentsGrid(grdEquipments);
            foreach (var list in _equRepo.Equipments.Where(n => n.InventoryNumber.Contains(txtFilterEquipments.Text)))
            {
                EquServices.AddToGrid(grdEquipments, list);
            }
            if (grdEquipments.Items.Count == 0)
            {
                EquServices.AddToGrid(grdEquipments, new Equipment() {Id = 0, EmployeeId = 0, Employee = _tmpEmployee, Condition = Equipment.ConditionOfEquipment.Bad, InventoryNumber = "нет такого", NameEquipment = "нет такого" });
            }
        }

        private void updateEquipment_Click(object sender, EventArgs e)
        {
            if (_tmpEquipment.NameEquipment != txtNameEquipment.Text)
            {
                _tmpEquipment.NameEquipment = txtNameEquipment.Text;
            }
            if (_tmpEquipment.InventoryNumber != txtInventoryNumberEquipment.Text)
            {
                _tmpEquipment.InventoryNumber = txtInventoryNumberEquipment.Text;
            }
            _tmpEquipment.Condition = (Equipment.ConditionOfEquipment)(cBoxCondition.SelectedIndex + 1);
            _tmpEquipment.Employee = _cmbEmployee;
            _equRepo.Update(_tmpEquipment);
            RefreshEquipmentsGrid();
        }

        private void cmbBoxEmployeers_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] str = cmbBoxEmployeersForEquipments.Text.Split(' ');
            _cmbEmployee = _empRepo.Employees.Find(x => x.EmployeeNumber == str[3]);
        }

        private void addEquipment_Click(object sender, EventArgs e)
        {

            if (txtInventoryNumberEquipment.Text != "" && txtNameEquipment.Text != "")
            {
                _equRepo.Add(new Equipment() {NameEquipment = txtNameEquipment.Text , InventoryNumber = txtInventoryNumberEquipment.Text, Employee = _cmbEmployee , Condition = (Equipment.ConditionOfEquipment)(cBoxCondition.SelectedIndex + 1) });
                RefreshEquipmentsGrid();
            }
            else
            {
                MessageBox.Show("Оборудование с таким инвентарным номером уже есть!!!", "Ошибка в исходных данных", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void deleteEquipment_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно желаете удалить запись?", "Удаление записи", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _equRepo.Delete(_tmpEquipment);
                RefreshEquipmentsGrid();
            }
        }
        #endregion

        #region MaterialAction
        private void txtFilterMaterials_TextChanged(object sender, EventArgs e)
        {
            MatServices.ClearMaterialsGrid(grdMaterials, 0);
            foreach (var list in _matRepo.Materials.Where(n => n.NameMaterial.Contains(txtFilterMaterials.Text)))
            {
                MatServices.AddToGrid(grdMaterials, list);
            }
            if (grdMaterials.Items.Count == 0)
            {
                MatServices.AddToGrid(grdMaterials, new Material() { Id = 0, NameMaterial = "нет такого", Quantity = 0 });
            }
        }

        private void updateMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                if (_tmpMaterial.NameMaterial != txtNameMaterial.Text)
                {
                    _tmpMaterial.NameMaterial = txtNameMaterial.Text;
                }
                if (_tmpMaterial.UnitOfMeasure != txtEdIzmMaterial.Text)
                {
                    _tmpMaterial.UnitOfMeasure = txtEdIzmMaterial.Text;
                }

                if (_tmpMaterial.Quantity != double.Parse(txtQuantityMaterial.Text))
                {
                    _tmpMaterial.Quantity = double.Parse(txtQuantityMaterial.Text);
                }
                    _tmpMaterial.Employee = _cmbEmployee;
                    _matRepo.Update(_tmpMaterial);
                    RefreshMaterialsGrid(grdMaterials, 0);

            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message, "Ошибка ввода кол-ва материала", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addMaterial_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNameMaterial.Text != "" && txtQuantityMaterial.Text != "" && txtQuantityMaterial.Text != "")
                {
                    _matRepo.Add(new Material() { NameMaterial = txtNameMaterial.Text, Quantity = double.Parse(txtQuantityMaterial.Text), UnitOfMeasure = txtEdIzmMaterial.Text, Employee = _cmbEmployee });
                    RefreshMaterialsGrid(grdMaterials, 0);
                }
                else
                {
                    MessageBox.Show("Ошибка данных", "Добавление нового материалы",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message, "Ошибка ввода кол-ва материала", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbBoxEmployeersForMaterials_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] str = cmbBoxEmployeersForMaterials.Text.Split(' ');
            _cmbEmployee = _empRepo.Employees.Find(x => x.EmployeeNumber == str[3]);
        }

        private void deleteMaterial_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно желаете удалить запись?", "Удаление записи", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _matRepo.Delete(_tmpMaterial);
                RefreshMaterialsGrid(grdMaterials, 0);
            }
        }


        #endregion

        #region PartAction
        private void txtFilterParts_TextChanged(object sender, EventArgs e)
        {
            PartServices.ClearPartsGrid(grdParts,0);
            foreach (var list in _partRepo.Parts.Where(n => n.NamePart.Contains(txtFilterParts.Text)))
            {
                PartServices.AddToGrid(grdParts, list);
            }
            if (grdParts.Items.Count == 0)
            {
                PartServices.AddToGrid(grdParts, new Part() { Id = 0, NamePart = "нет такого", PartNumber = "нет такого", Quantity = 0 });
            }
        }

        private void updatePart_Click(object sender, EventArgs e)
        {
            try
            {
                if (_tmpPart.NamePart != txtNamePart.Text)
                {
                    _tmpPart.NamePart = txtNamePart.Text;
                }
                if (_tmpPart.PartNumber != txtPartNumber.Text)
                {
                    _tmpPart.PartNumber = txtPartNumber.Text;
                }
                if ( _tmpPart.Quantity != double.Parse(txtQuantityPart.Text))
                {
                    _tmpPart.Quantity = double.Parse(txtQuantityPart.Text);
                }
                if (_tmpPart.UnitOfMeasure != txtEdIzmPart.Text)
                {
                    _tmpPart.UnitOfMeasure = txtEdIzmPart.Text;
                }
                _tmpPart.Employee = _cmbEmployee;
                    _partRepo.Update(_tmpPart);
                    RefreshPartsGrid(grdParts,0);
            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message, "Ошибка ввода кол-ва материала", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbBoxEmployeersForParts_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] str = cmbBoxEmployeersForParts.Text.Split(' ');
            _cmbEmployee = _empRepo.Employees.Find(x => x.EmployeeNumber == str[3]);
        }

        private void addPart_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNamePart.Text != "" && txtPartNumber.Text != "" && txtQuantityPart.Text!="" && txtEdIzmPart.Text !="")
                {
                    _partRepo.Add(new Part() { NamePart = txtNamePart.Text, PartNumber = txtPartNumber.Text, Employee = _cmbEmployee, Quantity = double.Parse(txtQuantityPart.Text), UnitOfMeasure = txtEdIzmPart.Text });
                    RefreshPartsGrid(grdParts, 0);
                }
                else
                {
                    MessageBox.Show("Ошибка данных", "Добавление нового материалы",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
            catch (FormatException exception)
            {
                MessageBox.Show(exception.Message, "Ошибка ввода кол-ва материала", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void deletePart_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно желаете удалить запись?", "Удаление записи", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                _partRepo.Delete(_tmpPart);
                RefreshPartsGrid(grdParts,0);
            }
        }


        #endregion

        private void repairForeignOrganization_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage6;
        }

        private void tabPage5_Enter(object sender, EventArgs e)
        {
            try
            {
                lblIdEquipmentRepair.ForeColor = Color.Crimson;
                lbNameEquipment.ForeColor = Color.Crimson;
                lbInventaryNumberEquipment.ForeColor = Color.Crimson;
                lbConditionEquipment.ForeColor = Color.Crimson;
                lbEmployeeEquipment.ForeColor = Color.Crimson;

                dateTimePicker1.Value = DateTime.Now;

                lblIdEquipmentRepair.Text = _tmpEquipment.Id.ToString();
                lbNameEquipment.Text = _tmpEquipment.NameEquipment;
                lbInventaryNumberEquipment.Text = _tmpEquipment.InventoryNumber;
                lbConditionEquipment.Text = _tmpEquipment.Condition.ToString();
                lbEmployeeEquipment.Text = _tmpEquipment.Employee.EmployeeNumber + "\n" + _tmpEquipment.Employee.FirstName + "\n" + _tmpEquipment.Employee.LastName + "\n" + _tmpEquipment.Employee.Patronymic;
                RefreshMaterialsGrid(grdMaterialsForRepair, 1);
                RefreshPartsGrid(grdPartsForRepair, 1);

                //_tmpEmployee = new Employee() {FirstName = "a", LastName = "b", };
            }
            catch (NullReferenceException )
            {
                if (MessageBox.Show("Оборудование не выбрано! \n Будете выбирать?", "Ошибка", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    RefreshEquipmentsGrid();
                    tabControl1.SelectedTab = tabPage2;
                }
                else
                {
                    tabControl1.SelectedTab = tabPage1;
                }
            }
        }

        private void txtFilterMaterialForRepair_TextChanged(object sender, EventArgs e)
        {
            MatServices.ClearMaterialsGrid(grdMaterialsForRepair, 1);
            foreach (var list in _matRepo.Materials.Where(n => n.NameMaterial.Contains(txtFilterMaterialForRepair.Text)))
            {
                MatServices.AddToGrid(grdMaterialsForRepair, list);
            }
            if (grdMaterialsForRepair.Items.Count == 0)
            {
                MatServices.AddToGrid(grdMaterialsForRepair, new Material() { Id = 0, NameMaterial = "нет такого", Quantity = 0 });
            }
        }

        private void txtFilterPartForRepair_TextChanged(object sender, EventArgs e)
        {
            PartServices.ClearPartsGrid(grdPartsForRepair, 1);
            foreach (var list in _partRepo.Parts.Where(n => n.NamePart.Contains(txtFilterPartForRepair.Text)))
            {
                PartServices.AddToGrid(grdPartsForRepair, list);
            }
            if (grdPartsForRepair.Items.Count == 0)
            {
                PartServices.AddToGrid(grdPartsForRepair, new Part() { Id = 0, NamePart = "нет такого", PartNumber = "нет такого", Quantity = 0 });
            }
        }

        private void grdMaterialsForRepair_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdMaterialsForRepair.SelectedItems.Count > 0)
            {
                lblIdMaterialRepair.Text = grdMaterialsForRepair.SelectedItems[0].Text;
                _tmpMaterial = _matRepo.Materials.Find(x => x.Id == Convert.ToInt32(lblIdMaterialRepair.Text));
                lblCountMaterialForRepair.Text = _tmpMaterial.Quantity.ToString();
                txtCountMaterialForRepair.Text = _tmpMaterial.Quantity.ToString();
            }
        }

        private void grdPartsForRepair_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdPartsForRepair.SelectedItems.Count > 0)
            {
                lblIdPartRepair.Text = grdPartsForRepair.SelectedItems[0].Text;
                _tmpPart = _partRepo.Parts.Find(x => x.Id == Convert.ToInt32(lblIdPartRepair.Text));
                lblCountPartForRepair.Text = _tmpPart.Quantity.ToString();
                txtCountPartForRepair.Text = _tmpPart.Quantity.ToString();
            }
        }

        private void addMaterialForRepair_Click(object sender, EventArgs e)
        {
            if (_materialsIdList.Find(t => t == _tmpMaterial.Id) == 0)
               {
                try
                {
                    if (txtCountMaterialForRepair.Text != "")
                    {
                        var descinationCount = double.Parse(txtCountMaterialForRepair.Text);
                        var sourceСount = double.Parse(lblCountMaterialForRepair.Text);
                        if (sourceСount >= descinationCount && descinationCount > 0)
                        {
                            _materialsIdList.Add(_tmpMaterial.Id);
                            listAddMaterialForRepair.Items.Add(_tmpMaterial.Id + " " + _tmpMaterial.NameMaterial + " "+ _tmpMaterial.UnitOfMeasure + " " + txtCountMaterialForRepair.Text);
                        }
                        else
                        {
                            MessageBox.Show("Данного материала нет в таком количестве", "Ошибка с количеством материала",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Вы не ввели количество добавляемого материала", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Некорректный ввод количества добавляемого материала", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCountMaterialForRepair.Clear();
                }
               }
            else
            {
                MessageBox.Show("Такой материал уже есть в списке", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addPartForRepair_Click(object sender, EventArgs e)
        {
            if (_partsIdList.Find(t => t == _tmpPart.Id) == 0)
            {
                try
                {
                    if (txtCountPartForRepair.Text != "")
                    {
                        var descinationCount = double.Parse(txtCountPartForRepair.Text);
                        var sourceСount = double.Parse(lblCountPartForRepair.Text);
                        if (sourceСount >= descinationCount && descinationCount > 0)
                        {
                            _partsIdList.Add(_tmpPart.Id);
                            listAddPartForRepair.Items.Add(_tmpPart.Id + " " + _tmpPart.NamePart + " " + _tmpPart.PartNumber + " " +_tmpPart.UnitOfMeasure+ " " + txtCountPartForRepair.Text);
                        }
                        else
                        {
                            MessageBox.Show("Данного материала нет в таком количестве", "Ошибка с количеством материала",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Вы не ввели количество добавляемого материала", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Некорректный ввод количества добавляемого материала", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCountPartForRepair.Clear();
                }
            }
            else
            {
                MessageBox.Show("Такой материал уже есть в списке", "Ошибка ввода", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listAddMaterialForRepair.Items.Count > 0)
            {
                try
                {
                    string[] str = listAddMaterialForRepair.Items[listAddMaterialForRepair.SelectedIndex].ToString().Split(' ');
                    int index = int.Parse(str[0]);
                    _materialsIdList.Remove(index);
                    listAddMaterialForRepair.Items.RemoveAt(listAddMaterialForRepair.SelectedIndex);

                }
                catch
                {
                    MessageBox.Show("Что-то пошло не так. Не сохраняйте изменения", "Ошибка выполнения программы", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listAddPartForRepair.Items.Count > 0)
            {
                try
                {
                    string[] str = listAddPartForRepair.Items[listAddPartForRepair.SelectedIndex].ToString().Split(' ');
                    int index = int.Parse(str[0]);
                    _partsIdList.Remove(index);
                    listAddPartForRepair.Items.RemoveAt(listAddPartForRepair.SelectedIndex);
                }
                catch
                {
                    MessageBox.Show("Что-то пошло не так. Не сохраняйте изменения", "Ошибка выполнения программы",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btnSaveHardwareRepair_Click(object sender, EventArgs e)
        {
            if (listAddMaterialForRepair.Items.Count > 0 || listAddPartForRepair.Items.Count > 0)
            {
                _tmpHardwareRepair = new HardwareRepair() { Employee = _tmpEquipment.Employee, DateReplaced = dateTimePicker1.Value, Equipment = _tmpEquipment, Сomment = txtCommentsForHardwareRepair.Text };
                _hardRepo.Add(_tmpHardwareRepair);
                if (listAddMaterialForRepair.Items.Count > 0)
                {
                    foreach (var list in listAddMaterialForRepair.Items)
                    {
                        var str = list.ToString().Split(' ');
                        _matRepairdRepo.Add(new MaterialRepair()
                        {
                            HardwareRepair = _tmpHardwareRepair,
                            NameMaterial = str[1],
                            UnitOfMeasure = str[2],
                            Quantity = double.Parse(str[3])
                        });
                        Material tmp = _matRepo.Materials.Find(n => n.Id == int.Parse(str[0]));
                        tmp.Quantity = tmp.Quantity - double.Parse(str[3]);
                        _matRepo.Update(tmp);
                        _matRepo.Delete(_matRepo.Materials.Find(n => n.Quantity == 0));
                    }
                   
                    RefreshMaterialsGrid(grdMaterialsForRepair, 1);
                }
                if (listAddPartForRepair.Items.Count > 0)
                {
                    foreach (var list in listAddPartForRepair.Items)
                    {
                        var str = list.ToString().Split(' ');
                        _partRepairdRepo.Add(new PartRepair()
                        {
                            HardwareRepair = _tmpHardwareRepair,
                            NamePart = str[1],
                            PartNumber = str[2],
                            UnitOfMeasure = str[3],
                            Quantity = double.Parse(str[4])
                        });
                        Part tmp = _partRepo.Parts.Find(n => n.Id == int.Parse(str[0]));
                        tmp.Quantity = tmp.Quantity - double.Parse(str[4]);
                        _partRepo.Update(tmp);
                        _partRepo.Delete(_partRepo.Parts.Find(n => n.Quantity == 0));
                    }
                    RefreshPartsGrid(grdPartsForRepair, 1);
                }
                listAddMaterialForRepair.Items.Clear();
                listAddPartForRepair.Items.Clear();
            }
            else
            {
                MessageBox.Show("Нет данных для обработки...", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (_tmpHardwareRepair != null && _tmpEmployee !=null)
            {
                FrmReportRepair frmView = new FrmReportRepair(_tmpHardwareRepair, _tmpEquipment, _tmpEmployee, dateTimePicker1.Value);
                frmView.ShowDialog();
            }
            else
            {
                MessageBox.Show("Для построения отчета необходим сохранить введенные данные", "Ошибка построения отчета", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
       
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (_tmpEquipment != null)
            {

                FrmReportViewEquipment frmReport = new FrmReportViewEquipment();
                frmReport.ShowDialog();
            }
            else
            {
                MessageBox.Show("Для начала выберите оборудование", "Ошибка в исходных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (_tmpEmployee != null)
            {
                FrmReportPC frmReportPc = new FrmReportPC(_tmpEmployee);
                frmReportPc.ShowDialog();
            }
            else
            {
                MessageBox.Show("Для начала выберите сотрудника", "Ошибка в исходных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button8_Click_1(object sender, EventArgs e)
        {

            if (_tmpEmployee != null)
            {
                FrmReportMaterial frmMaterial = new FrmReportMaterial(_tmpEmployee);
                frmMaterial.ShowDialog();
            }
            else
            {
                MessageBox.Show("Для начала выберите сотрудника", "Ошибка в исходных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button9_Click_1(object sender, EventArgs e)
        {

            if (_tmpEmployee != null)
            {
                FrmReportParts frmParts = new FrmReportParts(_tmpEmployee);
                frmParts.ShowDialog();
            }
            else
            {
                MessageBox.Show("Для начала выберите сотрудника", "Ошибка в исходных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {

            if (_tmpEmployee != null)
            {
                FrmAllForEmployee frmAll = new FrmAllForEmployee(_tmpEmployee);
                frmAll.ShowDialog();
            }
            else
            {
                MessageBox.Show("Для начала выберите сотрудника", "Ошибка в исходных данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tabPage6_Enter(object sender, EventArgs e)
        {
            try
            {
                lblIdEquipmentRepairAccount.ForeColor = Color.Crimson;
                lbNameEquipmentAccount.ForeColor = Color.Crimson;
                lbInventaryNumberEquipmentAccount.ForeColor = Color.Crimson;
                lbConditionEquipmentAccount.ForeColor = Color.Crimson;
                lbEmployeeEquipmentAccount.ForeColor = Color.Crimson;

                dateAccountingRepair.Value = DateTime.Now;

                lblIdEquipmentRepairAccount.Text = _tmpEquipment.Id.ToString();
                lbNameEquipmentAccount.Text = _tmpEquipment.NameEquipment;
                lbInventaryNumberEquipmentAccount.Text = _tmpEquipment.InventoryNumber;
                lbConditionEquipmentAccount.Text = _tmpEquipment.Condition.ToString();
                lbEmployeeEquipmentAccount.Text = _tmpEquipment.Employee.EmployeeNumber + "\n" + _tmpEquipment.Employee.FirstName + "\n" + _tmpEquipment.Employee.LastName + "\n" + _tmpEquipment.Employee.Patronymic;

                RefreshOrganizationGrid();

                //_tmpEmployee = new Employee() {FirstName = "a", LastName = "b", };
            }
            catch (NullReferenceException)
            {
                if (MessageBox.Show("Оборудование не выбрано! \n Будете выбирать?", "Ошибка", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    RefreshEquipmentsGrid();
                    tabControl1.SelectedTab = tabPage2;
                }
                else
                {
                    tabControl1.SelectedTab = tabPage1;
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
        }

        private void btnSaveAccountingRepair_Click_Click(object sender, EventArgs e)
        {
            if (_tmpOrganization != null)
            {
                if (txtWhatWork.Text != "" && txtCostWhatWork.Text != "")
                {
                    try
                    {
                        _accountingRepairRepo.Add(new AccountingRepair()
                        {
                            Organization = _tmpOrganization,
                            Equipment = _tmpEquipment,
                            DateReplaced = dateAccountingRepair.Value,
                            WhatWork = txtWhatWork.Text,
                            Сost = Double.Parse(txtCostWhatWork.Text)
                        });
                        MessageBox.Show("Данные соранены в БД", "Сохранение данных", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        txtWhatWork.Text = "";
                        txtCostWhatWork.Text = "";
                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("Ошибка в указании стоимости работ (разделитель дробной части)", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                   
                }
                else
                {
                    MessageBox.Show("Не введены исходные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Не выбрана ремонтная организация", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
