﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HardwareInspection.Model;
using HardwareInspection.Repository;
using Microsoft.Reporting.WinForms;

namespace HardwareInspection
{
    public partial class FrmAllForEmployee : Form
    {
        static readonly Context DbContext = new Context();
        readonly PartRepository _partRepo = new PartRepository(DbContext);
        readonly EquipmentRepository _equRepo = new EquipmentRepository(DbContext);
        readonly MaterialRepository _matRepo = new MaterialRepository(DbContext);

        ReportDataSource _rep1, _rep2, _rep3;
        private Employee _emp;

        public FrmAllForEmployee(Employee emp)
        {
            InitializeComponent();
            _emp = emp;
        }

        private void FrmAllForEmployee_Load(object sender, EventArgs e)
        {
            DataTable table1 = GetTable(1);
            _rep1 = new ReportDataSource("DataSet1", table1);

            DataTable table2 = GetTable(2);
            _rep2 = new ReportDataSource("DataSet2", table2);

            DataTable table3 = GetTable(3);
            _rep3 = new ReportDataSource("DataSet3", table3);

            ReportParameter textbox1Param = new ReportParameter("ReportParameter1", _emp.LastName + " " + _emp.FirstName + " " + _emp.Patronymic);
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            ReportParameter textbox2Param = new ReportParameter("ReportParameter2", _emp.EmployeeNumber);
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            reportViewer1.LocalReport.SetParameters(textbox2Param);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(_rep1);
            reportViewer1.LocalReport.DataSources.Add(_rep2);
            reportViewer1.LocalReport.DataSources.Add(_rep3);
            reportViewer1.RefreshReport();

        }
        public DataTable GetTable(int id)
        {
            if (id == 1)
            {
                DataTable table = new DataTable();
                table.Columns.Add("Номер", typeof(string));
                table.Columns.Add("Наименование", typeof(string));
                table.Columns.Add("Инв_номер", typeof(string));
                int i = 0;
                foreach (var list in _equRepo.Equipments.Where(n => n.EmployeeId == _emp.Id))
                {
                    table.Rows.Add(++i, list.NameEquipment, list.InventoryNumber);
                }
                return table;
            }
            if (id == 2)
            {
                DataTable table = new DataTable();
                table.Columns.Add("Номер", typeof(string));
                table.Columns.Add("Наименование", typeof(string));
                table.Columns.Add("Единицы", typeof(string));
                table.Columns.Add("Количество", typeof(string));
                int i = 0;
                foreach (var list in _partRepo.Parts.Where(n => n.EmployeeId == _emp.Id))
                {
                    table.Rows.Add(++i, list.NamePart + "   " + list.PartNumber, list.UnitOfMeasure,  list.Quantity);
                }
                return table;
            }
            if (id == 3)
            {
                DataTable table = new DataTable();
                table.Columns.Add("Номер", typeof(string));
                table.Columns.Add("Наименование", typeof(string));
                table.Columns.Add("Единицы", typeof(string));
                table.Columns.Add("Количество", typeof(string));
                int i = 0;
                foreach (var list in _matRepo.Materials.Where(n => n.EmployeeId == _emp.Id))
                {
                    table.Rows.Add(++i, list.NameMaterial, list.UnitOfMeasure, list.Quantity);
                }
                return table;
            }
            return null;
            // Here we create a DataTable with four columns.

        }
    }
}
