﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Data.Entity;
using HardwareInspection.Model;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HardwareInspection.Repository;
using HardwareInspection.Services;

namespace HardwareInspection
{
    public partial class FrmWorkWithRepair : Form

    {
        static readonly Context DbContext = new Context();
        public AccountingRepairServices AccountingRepairService = new AccountingRepairServices();
        public HardwareRepairServices HardRepairService = new HardwareRepairServices();
        public EquipmentServices EquServices = new EquipmentServices();
        public MaterialServices MatServices = new MaterialServices();
        public PartServices PartServices = new PartServices();
        public OrganizationServices OrgServices = new OrganizationServices();
        readonly AccountingRepairRepository _accountingRepairRepo = new AccountingRepairRepository(DbContext);
        readonly HardwareRepairRepository _hardRepairRepo = new HardwareRepairRepository(DbContext);
        readonly OrganizationRepository _orgRepairdRepo = new OrganizationRepository(DbContext);
        readonly EquipmentRepository _equRepo = new EquipmentRepository(DbContext);
        readonly PartRepairRepository _partRepairdRepo = new PartRepairRepository(DbContext);
        readonly EmployeeRepository _empRepo = new EmployeeRepository(DbContext);
        readonly MaterialRepairRepository _matRepairdRepo = new MaterialRepairRepository(DbContext);
        private Organization _tmpOrganization;
        private Equipment _tmpEquipment;
        private AccountingRepair _tmpAccountingRepair;
        private HardwareRepair _tmpHardwareRepair;
        private Employee _tmpEmployee;

        public FrmWorkWithRepair()
        {
            InitializeComponent();
        }

        private void RefreshAccountingRepairGrid()
        {
            AccountingRepairService.ClearAccountingRepairGrid(grdAccountingRepair);
            
            try
            {
                foreach (var list in _accountingRepairRepo.AccountingRepairs)
                {
                    _tmpOrganization = _orgRepairdRepo.Organizations.Find(n => n.Id == list.OrganizationId);
                    AccountingRepairService.AddToGrid(grdAccountingRepair, list, _tmpOrganization);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void RefreshHardwareRepairGrid()
        {
            HardRepairService.ClearHardwareRepairsGrid(grdHardwareRepair);
            try
            {
                foreach (var list in _hardRepairRepo.HardwareRepairs)
                {
                    _tmpEquipment = _equRepo.Equipments.Find(n => n.Id == list.EquipmentId);
                    HardRepairService.AddToGrid(grdHardwareRepair, list, _tmpEquipment);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void FrmWorkWithRepair_Load(object sender, EventArgs e)
        {
            lblIdAc.Visible = false;
            lblIdHard.Visible = false;
            dateTimePicker1.Value = DateTime.Now.AddMonths(-1);
            dateTimePicker4.Value = DateTime.Now.AddMonths(-1);
            lblIdAc.Visible = false;
            lblIdHard.Visible = false;
            RefreshAccountingRepairGrid();
            RefreshHardwareRepairGrid();
        }

        private void grdAccountingRepair_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdAccountingRepair.SelectedItems.Count > 0)
            {
                lblIdAc.Text = grdAccountingRepair.SelectedItems[0].Text;
                _tmpAccountingRepair = _accountingRepairRepo.AccountingRepairs.Find(n => n.Id == Int16.Parse(lblIdAc.Text));
                EquServices.ClearEquipmentsGrid(listView1);
                foreach (var list in _equRepo.Equipments.Where(n => n.Id == _tmpAccountingRepair.EquipmentId))
                {
                    EquServices.AddToGrid(listView1, list);
                }
            }
        }

        private void grdHardwareRepair_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdHardwareRepair.SelectedItems.Count > 0)
            {
                lblIdHard.Text = grdHardwareRepair.SelectedItems[0].Text;
                _tmpHardwareRepair = _hardRepairRepo.HardwareRepairs.Find(n => n.Id == Int16.Parse(lblIdHard.Text));
                MatServices.ClearMaterialsGrid(grdMat,1);
                foreach (var list in _matRepairdRepo.MaterialRepairs.Where(n => n.HardwareRepairId == _tmpHardwareRepair.Id))
                {
                    MatServices.AddToGridRepairView(grdMat, list);
                }
                PartServices.ClearPartsGrid(grdParts,1);
                
                foreach (var list in _partRepairdRepo.PartRepairs.Where(n => n.HardwareRepairId == _tmpHardwareRepair.Id))
                {
                    PartServices.AddToGridRepairView(grdParts, list);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AccountingRepairService.ClearAccountingRepairGrid(grdAccountingRepair);

            try
            {
                foreach (var list in _accountingRepairRepo.AccountingRepairs.Where(n =>n.DateReplaced >= dateTimePicker1.Value && n.DateReplaced <= dateTimePicker2.Value))
                {
                    _tmpOrganization = _orgRepairdRepo.Organizations.Find(n => n.Id == list.OrganizationId);
                    AccountingRepairService.AddToGrid(grdAccountingRepair, list, _tmpOrganization);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            HardRepairService.ClearHardwareRepairsGrid(grdHardwareRepair);
            try
            {
                foreach (var list in _hardRepairRepo.HardwareRepairs.Where(n => n.DateReplaced >= dateTimePicker4.Value && n.DateReplaced <= dateTimePicker3.Value))
                {
                    _tmpEquipment = _equRepo.Equipments.Find(n => n.Id == list.EquipmentId);
                    HardRepairService.AddToGrid(grdHardwareRepair, list, _tmpEquipment);
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("NullReferenceException");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmHardRepairReport frm = new FrmHardRepairReport(dateTimePicker4.Value, dateTimePicker3.Value);
            frm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (lblIdHard.Text != "label4")
            {
                _tmpEmployee = _empRepo.Employees.Find(x => x.Id == Convert.ToInt32(_tmpHardwareRepair.EmployeeId));
                FrmReportRepair frmView = new FrmReportRepair(_tmpHardwareRepair, _tmpEquipment, _tmpEmployee, _tmpHardwareRepair.DateReplaced);
                frmView.ShowDialog();
            }
            else
            {
                MessageBox.Show("Выберите данные для печати акта", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmAccountRepairReport frm = new FrmAccountRepairReport(dateTimePicker1.Value, dateTimePicker2.Value);
            frm.ShowDialog();
        }
    }
}
