﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HardwareInspection.Model;
using HardwareInspection.Repository;
using Microsoft.Reporting.WinForms;

namespace HardwareInspection
{
    public partial class FrmReportMaterial : Form
    {
        static readonly Context DbContext = new Context();
        ReportDataSource _rep1;
        readonly MaterialRepository _matRepo = new MaterialRepository(DbContext);
        private Employee _emp;

        public FrmReportMaterial(Employee emp)
        {
            InitializeComponent();
            _emp = emp;
        }

        private void FrmReportMaterial_Load(object sender, EventArgs e)
        {
            DataTable table = GetTable();
            _rep1 = new ReportDataSource("DataSet2", table);
            ReportParameter textbox1Param = new ReportParameter("ReportParameter1", _emp.LastName + " " + _emp.FirstName + " " + _emp.Patronymic);
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            ReportParameter textbox2Param = new ReportParameter("ReportParameter2", _emp.EmployeeNumber);
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            reportViewer1.LocalReport.SetParameters(textbox2Param);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(_rep1);
            reportViewer1.RefreshReport();
        }

        public DataTable GetTable()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("Номер", typeof(string));
            table.Columns.Add("Наименование", typeof(string));
            table.Columns.Add("Единицы", typeof(string));
            table.Columns.Add("Количество", typeof(string));
            int i = 0;
            foreach (var list in _matRepo.Materials.Where(n => n.EmployeeId == _emp.Id))
            {
                table.Rows.Add(++i, list.NameMaterial, list.UnitOfMeasure, list.Quantity);
            }
            return table;
        }
    }
}
