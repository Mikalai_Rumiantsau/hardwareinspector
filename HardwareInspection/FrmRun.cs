﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HardwareInspection
{
    public partial class FrmRun : Form
    {
        public FrmRun()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Введите данные для входа", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if ((textBox1.Text == "Admin" && textBox2.Text == "Admin") || (textBox1.Text == "admin" && textBox2.Text == "admin")
                    || (textBox1.Text == "User" && textBox2.Text == "User") || (textBox1.Text == "user" && textBox2.Text == "user"))
                {
                    frmMain frm = new frmMain();
                    frm.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Пользователь не опознан", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox1.Clear();
                    textBox2.Clear();
                }
            }
        }
    }
}
