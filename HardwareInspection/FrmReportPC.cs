﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HardwareInspection.Model;
using HardwareInspection.Repository;
using Microsoft.Reporting.WinForms;

namespace HardwareInspection
{
    public partial class FrmReportPC : Form
    {
        static readonly Context DbContext = new Context();
        readonly EquipmentRepository _equRepo = new EquipmentRepository(DbContext);
        ReportDataSource _rep1;
        private Employee _emp;

        public FrmReportPC(Employee emp)
        {
            InitializeComponent();
            _emp = emp;
        }

        private void FrmReportPC_Load(object sender, EventArgs e)
        {
           
            DataTable table = GetTable();
            _rep1 = new ReportDataSource("DataSet2", table);
//
            ReportParameter textbox1Param = new ReportParameter("ReportParameter1", _emp.LastName + " " +_emp.FirstName + " " + _emp.Patronymic);
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            ReportParameter textbox2Param = new ReportParameter("ReportParameter2", _emp.EmployeeNumber);
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            reportViewer1.LocalReport.SetParameters(textbox2Param);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(_rep1);
            reportViewer1.RefreshReport();

        }

        public DataTable GetTable()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("Номер", typeof(string));
            table.Columns.Add("Наименование", typeof(string));
            table.Columns.Add("Инв_номер", typeof(string));
            int i = 0;
            foreach (var list in _equRepo.Equipments.Where(n => n.EmployeeId == _emp.Id))
            {
                table.Rows.Add(++i, list.NameEquipment, list.InventoryNumber);
            }
            return table;
        }
    }
}
