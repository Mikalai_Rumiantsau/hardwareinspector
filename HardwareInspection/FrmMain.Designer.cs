﻿using System.Windows.Forms;

namespace HardwareInspection
{
    partial class frmMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.работаСБДToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сотрудникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оборудованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.материалыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.запасныеЧастиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ремонтныеОрганизацииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ремонтыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.собственнымиСиламиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.стороннимиОрганизациямиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.просмотрпечатьИсторииРемонтовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTabNom = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.filterEmployee = new System.Windows.Forms.TextBox();
            this.lblIdEmployee = new System.Windows.Forms.Label();
            this.txtPatronymic = new System.Windows.Forms.TextBox();
            this.txtSoname = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.deleteEmployee = new System.Windows.Forms.Button();
            this.updateEmployee = new System.Windows.Forms.Button();
            this.addEmployee = new System.Windows.Forms.Button();
            this.grdEmployees = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.repairForeignOrganization = new System.Windows.Forms.Button();
            this.repairOnItsOwn = new System.Windows.Forms.Button();
            this.cmbBoxEmployeersForEquipments = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cBoxCondition = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFilterEquipments = new System.Windows.Forms.TextBox();
            this.lblIdEquipment = new System.Windows.Forms.Label();
            this.txtInventoryNumberEquipment = new System.Windows.Forms.TextBox();
            this.txtNameEquipment = new System.Windows.Forms.TextBox();
            this.deleteEquipment = new System.Windows.Forms.Button();
            this.updateEquipment = new System.Windows.Forms.Button();
            this.addEquipment = new System.Windows.Forms.Button();
            this.grdEquipments = new System.Windows.Forms.ListView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label38 = new System.Windows.Forms.Label();
            this.txtEdIzmMaterial = new System.Windows.Forms.TextBox();
            this.cmbBoxEmployeersForMaterials = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFilterMaterials = new System.Windows.Forms.TextBox();
            this.lblIdMaterial = new System.Windows.Forms.Label();
            this.txtQuantityMaterial = new System.Windows.Forms.TextBox();
            this.txtNameMaterial = new System.Windows.Forms.TextBox();
            this.deleteMaterial = new System.Windows.Forms.Button();
            this.updateMaterial = new System.Windows.Forms.Button();
            this.addMaterial = new System.Windows.Forms.Button();
            this.grdMaterials = new System.Windows.Forms.ListView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label39 = new System.Windows.Forms.Label();
            this.txtEdIzmPart = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPartNumber = new System.Windows.Forms.TextBox();
            this.cmbBoxEmployeersForParts = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtFilterParts = new System.Windows.Forms.TextBox();
            this.lblIdPart = new System.Windows.Forms.Label();
            this.txtQuantityPart = new System.Windows.Forms.TextBox();
            this.txtNamePart = new System.Windows.Forms.TextBox();
            this.deletePart = new System.Windows.Forms.Button();
            this.updatePart = new System.Windows.Forms.Button();
            this.addPart = new System.Windows.Forms.Button();
            this.grdParts = new System.Windows.Forms.ListView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.txtCommentsForHardwareRepair = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblCountPartForRepair = new System.Windows.Forms.Label();
            this.txtCountPartForRepair = new System.Windows.Forms.TextBox();
            this.listAddPartForRepair = new System.Windows.Forms.ListBox();
            this.listAddMaterialForRepair = new System.Windows.Forms.ListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label25 = new System.Windows.Forms.Label();
            this.lblCountMaterialForRepair = new System.Windows.Forms.Label();
            this.txtCountMaterialForRepair = new System.Windows.Forms.TextBox();
            this.btnSaveHardwareRepair = new System.Windows.Forms.Button();
            this.addPartForRepair = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbInventaryNumberEquipment = new System.Windows.Forms.Label();
            this.lbEmployeeEquipment = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lbConditionEquipment = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lbNameEquipment = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblIdEquipmentRepair = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.addMaterialForRepair = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.lblIdPartRepair = new System.Windows.Forms.Label();
            this.txtFilterPartForRepair = new System.Windows.Forms.TextBox();
            this.grdPartsForRepair = new System.Windows.Forms.ListView();
            this.lblIdMaterialRepair = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtFilterMaterialForRepair = new System.Windows.Forms.TextBox();
            this.grdMaterialsForRepair = new System.Windows.Forms.ListView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btnSaveAccountingRepair_Click = new System.Windows.Forms.Button();
            this.lblIdOrganizationForRepair = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtPhoneOrganizationForRepair = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtAdresOrganizationForRepair = new System.Windows.Forms.TextBox();
            this.txtNameOrganizationForRepair = new System.Windows.Forms.TextBox();
            this.deleteOrganization = new System.Windows.Forms.Button();
            this.updateOrganization = new System.Windows.Forms.Button();
            this.addOrganization = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.txtFilterOrganizationForRepair = new System.Windows.Forms.TextBox();
            this.grdOrganizationForRepair = new System.Windows.Forms.ListView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtCostWhatWork = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtWhatWork = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.dateAccountingRepair = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbInventaryNumberEquipmentAccount = new System.Windows.Forms.Label();
            this.lbEmployeeEquipmentAccount = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.lbConditionEquipmentAccount = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lbNameEquipmentAccount = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lblIdEquipmentRepairAccount = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(838, 1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "InitData";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.работаСБДToolStripMenuItem,
            this.ремонтыToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1034, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // работаСБДToolStripMenuItem
            // 
            this.работаСБДToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сотрудникиToolStripMenuItem,
            this.оборудованиеToolStripMenuItem,
            this.материалыToolStripMenuItem,
            this.запасныеЧастиToolStripMenuItem,
            this.ремонтныеОрганизацииToolStripMenuItem});
            this.работаСБДToolStripMenuItem.Name = "работаСБДToolStripMenuItem";
            this.работаСБДToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.работаСБДToolStripMenuItem.Text = "Работа с БД";
            // 
            // сотрудникиToolStripMenuItem
            // 
            this.сотрудникиToolStripMenuItem.Name = "сотрудникиToolStripMenuItem";
            this.сотрудникиToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.сотрудникиToolStripMenuItem.Text = "Сотрудники";
            this.сотрудникиToolStripMenuItem.Click += new System.EventHandler(this.сотрудникиToolStripMenuItem_Click);
            // 
            // оборудованиеToolStripMenuItem
            // 
            this.оборудованиеToolStripMenuItem.Name = "оборудованиеToolStripMenuItem";
            this.оборудованиеToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.оборудованиеToolStripMenuItem.Text = "Оборудование";
            this.оборудованиеToolStripMenuItem.Click += new System.EventHandler(this.оборудованиеToolStripMenuItem_Click);
            // 
            // материалыToolStripMenuItem
            // 
            this.материалыToolStripMenuItem.Name = "материалыToolStripMenuItem";
            this.материалыToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.материалыToolStripMenuItem.Text = "Материалы";
            this.материалыToolStripMenuItem.Click += new System.EventHandler(this.материалыToolStripMenuItem_Click);
            // 
            // запасныеЧастиToolStripMenuItem
            // 
            this.запасныеЧастиToolStripMenuItem.Name = "запасныеЧастиToolStripMenuItem";
            this.запасныеЧастиToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.запасныеЧастиToolStripMenuItem.Text = "Запасные части";
            this.запасныеЧастиToolStripMenuItem.Click += new System.EventHandler(this.запасныеЧастиToolStripMenuItem_Click);
            // 
            // ремонтныеОрганизацииToolStripMenuItem
            // 
            this.ремонтныеОрганизацииToolStripMenuItem.Name = "ремонтныеОрганизацииToolStripMenuItem";
            this.ремонтныеОрганизацииToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.ремонтныеОрганизацииToolStripMenuItem.Text = "Ремонтные организации";
            this.ремонтныеОрганизацииToolStripMenuItem.Click += new System.EventHandler(this.ремонтныеОрганизацииToolStripMenuItem_Click);
            // 
            // ремонтыToolStripMenuItem
            // 
            this.ремонтыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.собственнымиСиламиToolStripMenuItem,
            this.стороннимиОрганизациямиToolStripMenuItem,
            this.просмотрпечатьИсторииРемонтовToolStripMenuItem});
            this.ремонтыToolStripMenuItem.Name = "ремонтыToolStripMenuItem";
            this.ремонтыToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.ремонтыToolStripMenuItem.Text = "Ремонты";
            // 
            // собственнымиСиламиToolStripMenuItem
            // 
            this.собственнымиСиламиToolStripMenuItem.Name = "собственнымиСиламиToolStripMenuItem";
            this.собственнымиСиламиToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.собственнымиСиламиToolStripMenuItem.Text = "Собственными силами";
            this.собственнымиСиламиToolStripMenuItem.Click += new System.EventHandler(this.собственнымиСиламиToolStripMenuItem_Click);
            // 
            // стороннимиОрганизациямиToolStripMenuItem
            // 
            this.стороннимиОрганизациямиToolStripMenuItem.Name = "стороннимиОрганизациямиToolStripMenuItem";
            this.стороннимиОрганизациямиToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.стороннимиОрганизациямиToolStripMenuItem.Text = "Сторонними организациями";
            this.стороннимиОрганизациямиToolStripMenuItem.Click += new System.EventHandler(this.стороннимиОрганизациямиToolStripMenuItem_Click);
            // 
            // просмотрпечатьИсторииРемонтовToolStripMenuItem
            // 
            this.просмотрпечатьИсторииРемонтовToolStripMenuItem.Name = "просмотрпечатьИсторииРемонтовToolStripMenuItem";
            this.просмотрпечатьИсторииРемонтовToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.просмотрпечатьИсторииРемонтовToolStripMenuItem.Text = "Просмотр/печать истории ремонтов";
            this.просмотрпечатьИсторииРемонтовToolStripMenuItem.Click += new System.EventHandler(this.просмотрпечатьИсторииРемонтовToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl1.Location = new System.Drawing.Point(3, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1031, 355);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.button8);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtTabNom);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.filterEmployee);
            this.tabPage1.Controls.Add(this.lblIdEmployee);
            this.tabPage1.Controls.Add(this.txtPatronymic);
            this.tabPage1.Controls.Add(this.txtSoname);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Controls.Add(this.deleteEmployee);
            this.tabPage1.Controls.Add(this.updateEmployee);
            this.tabPage1.Controls.Add(this.addEmployee);
            this.tabPage1.Controls.Add(this.grdEmployees);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1023, 329);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Работа с базой сотрудников";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Enter += new System.EventHandler(this.tabPage1_Enter);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(519, 284);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(256, 23);
            this.button10.TabIndex = 94;
            this.button10.Text = "Вывод на печать всего на сотруднике";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(519, 226);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(256, 23);
            this.button9.TabIndex = 93;
            this.button9.Text = "Вывод на печать запчастей на сотруднике";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(519, 255);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(256, 23);
            this.button8.TabIndex = 78;
            this.button8.Text = "Вывод на печать материалов на сотруднике";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(519, 197);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(256, 23);
            this.button7.TabIndex = 51;
            this.button7.Text = "Вывод на печать ПК на сотруднике";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(516, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 50;
            this.label5.Text = "Таб. ном:";
            // 
            // txtTabNom
            // 
            this.txtTabNom.Location = new System.Drawing.Point(580, 171);
            this.txtTabNom.Name = "txtTabNom";
            this.txtTabNom.Size = new System.Drawing.Size(100, 20);
            this.txtTabNom.TabIndex = 49;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(516, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 48;
            this.label4.Text = "Отчество:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(516, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 47;
            this.label3.Text = "Фамилия:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(516, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "Имя:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Введите часть фамилии для фильтра:";
            // 
            // filterEmployee
            // 
            this.filterEmployee.Location = new System.Drawing.Point(214, 11);
            this.filterEmployee.Name = "filterEmployee";
            this.filterEmployee.Size = new System.Drawing.Size(149, 20);
            this.filterEmployee.TabIndex = 44;
            this.filterEmployee.TextChanged += new System.EventHandler(this.filterEmployee_TextChanged);
            // 
            // lblIdEmployee
            // 
            this.lblIdEmployee.AutoSize = true;
            this.lblIdEmployee.Location = new System.Drawing.Point(831, 10);
            this.lblIdEmployee.Name = "lblIdEmployee";
            this.lblIdEmployee.Size = new System.Drawing.Size(35, 13);
            this.lblIdEmployee.TabIndex = 43;
            this.lblIdEmployee.Text = "label1";
            // 
            // txtPatronymic
            // 
            this.txtPatronymic.Location = new System.Drawing.Point(580, 136);
            this.txtPatronymic.Name = "txtPatronymic";
            this.txtPatronymic.Size = new System.Drawing.Size(100, 20);
            this.txtPatronymic.TabIndex = 42;
            // 
            // txtSoname
            // 
            this.txtSoname.Location = new System.Drawing.Point(580, 97);
            this.txtSoname.Name = "txtSoname";
            this.txtSoname.Size = new System.Drawing.Size(100, 20);
            this.txtSoname.TabIndex = 41;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(580, 59);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 40;
            // 
            // deleteEmployee
            // 
            this.deleteEmployee.Location = new System.Drawing.Point(700, 134);
            this.deleteEmployee.Name = "deleteEmployee";
            this.deleteEmployee.Size = new System.Drawing.Size(75, 23);
            this.deleteEmployee.TabIndex = 39;
            this.deleteEmployee.Text = "Удалить";
            this.deleteEmployee.UseVisualStyleBackColor = true;
            this.deleteEmployee.Click += new System.EventHandler(this.deleteEmployee_Click);
            // 
            // updateEmployee
            // 
            this.updateEmployee.Location = new System.Drawing.Point(700, 95);
            this.updateEmployee.Name = "updateEmployee";
            this.updateEmployee.Size = new System.Drawing.Size(75, 23);
            this.updateEmployee.TabIndex = 38;
            this.updateEmployee.Text = "Изменить";
            this.updateEmployee.UseVisualStyleBackColor = true;
            this.updateEmployee.Click += new System.EventHandler(this.updateEmployee_Click);
            // 
            // addEmployee
            // 
            this.addEmployee.Location = new System.Drawing.Point(700, 56);
            this.addEmployee.Name = "addEmployee";
            this.addEmployee.Size = new System.Drawing.Size(75, 23);
            this.addEmployee.TabIndex = 37;
            this.addEmployee.Text = "Добавить";
            this.addEmployee.UseVisualStyleBackColor = true;
            this.addEmployee.Click += new System.EventHandler(this.addEmployee_Click);
            // 
            // grdEmployees
            // 
            this.grdEmployees.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdEmployees.FullRowSelect = true;
            this.grdEmployees.GridLines = true;
            this.grdEmployees.Location = new System.Drawing.Point(9, 59);
            this.grdEmployees.Name = "grdEmployees";
            this.grdEmployees.Size = new System.Drawing.Size(489, 262);
            this.grdEmployees.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdEmployees.TabIndex = 36;
            this.grdEmployees.UseCompatibleStateImageBehavior = false;
            this.grdEmployees.View = System.Windows.Forms.View.Details;
            this.grdEmployees.SelectedIndexChanged += new System.EventHandler(this.grdEmployees_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.repairForeignOrganization);
            this.tabPage2.Controls.Add(this.repairOnItsOwn);
            this.tabPage2.Controls.Add(this.cmbBoxEmployeersForEquipments);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.cBoxCondition);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.txtFilterEquipments);
            this.tabPage2.Controls.Add(this.lblIdEquipment);
            this.tabPage2.Controls.Add(this.txtInventoryNumberEquipment);
            this.tabPage2.Controls.Add(this.txtNameEquipment);
            this.tabPage2.Controls.Add(this.deleteEquipment);
            this.tabPage2.Controls.Add(this.updateEquipment);
            this.tabPage2.Controls.Add(this.addEquipment);
            this.tabPage2.Controls.Add(this.grdEquipments);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1023, 329);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Работа с базой оборудования";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Enter += new System.EventHandler(this.tabPage2_Enter);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(518, 288);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(350, 23);
            this.button6.TabIndex = 66;
            this.button6.Text = "Вывод на печать оборудования тредующего ремонта";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // repairForeignOrganization
            // 
            this.repairForeignOrganization.Location = new System.Drawing.Point(518, 253);
            this.repairForeignOrganization.Name = "repairForeignOrganization";
            this.repairForeignOrganization.Size = new System.Drawing.Size(350, 23);
            this.repairForeignOrganization.TabIndex = 65;
            this.repairForeignOrganization.Text = "Оформить ремонт \"сторонней организации\"";
            this.repairForeignOrganization.UseVisualStyleBackColor = true;
            this.repairForeignOrganization.Click += new System.EventHandler(this.repairForeignOrganization_Click);
            // 
            // repairOnItsOwn
            // 
            this.repairOnItsOwn.Location = new System.Drawing.Point(518, 217);
            this.repairOnItsOwn.Name = "repairOnItsOwn";
            this.repairOnItsOwn.Size = new System.Drawing.Size(350, 23);
            this.repairOnItsOwn.TabIndex = 64;
            this.repairOnItsOwn.Text = "Оформить ремонт \"собственными силами\"";
            this.repairOnItsOwn.UseVisualStyleBackColor = true;
            this.repairOnItsOwn.Click += new System.EventHandler(this.repairOnItsOwn_Click);
            // 
            // cmbBoxEmployeersForEquipments
            // 
            this.cmbBoxEmployeersForEquipments.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbBoxEmployeersForEquipments.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBoxEmployeersForEquipments.FormattingEnabled = true;
            this.cmbBoxEmployeersForEquipments.Location = new System.Drawing.Point(605, 177);
            this.cmbBoxEmployeersForEquipments.Name = "cmbBoxEmployeersForEquipments";
            this.cmbBoxEmployeersForEquipments.Size = new System.Drawing.Size(263, 21);
            this.cmbBoxEmployeersForEquipments.TabIndex = 63;
            this.cmbBoxEmployeersForEquipments.SelectedIndexChanged += new System.EventHandler(this.cmbBoxEmployeers_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(515, 181);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 62;
            this.label10.Text = "Сотрудник:";
            // 
            // cBoxCondition
            // 
            this.cBoxCondition.FormattingEnabled = true;
            this.cBoxCondition.Items.AddRange(new object[] {
            "Good",
            "Bad"});
            this.cBoxCondition.Location = new System.Drawing.Point(605, 135);
            this.cBoxCondition.Name = "cBoxCondition";
            this.cBoxCondition.Size = new System.Drawing.Size(100, 21);
            this.cBoxCondition.TabIndex = 61;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(515, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 60;
            this.label6.Text = "Состояние:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(515, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 59;
            this.label7.Text = "Инв.номер:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(515, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 13);
            this.label8.TabIndex = 58;
            this.label8.Text = "Наименование:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(264, 13);
            this.label9.TabIndex = 57;
            this.label9.Text = "Введите часть инвентарного номера для фильтра:";
            // 
            // txtFilterEquipments
            // 
            this.txtFilterEquipments.Location = new System.Drawing.Point(276, 11);
            this.txtFilterEquipments.Name = "txtFilterEquipments";
            this.txtFilterEquipments.Size = new System.Drawing.Size(149, 20);
            this.txtFilterEquipments.TabIndex = 56;
            this.txtFilterEquipments.TextChanged += new System.EventHandler(this.txtFilterEquipments_TextChanged);
            // 
            // lblIdEquipment
            // 
            this.lblIdEquipment.AutoSize = true;
            this.lblIdEquipment.Location = new System.Drawing.Point(833, 8);
            this.lblIdEquipment.Name = "lblIdEquipment";
            this.lblIdEquipment.Size = new System.Drawing.Size(35, 13);
            this.lblIdEquipment.TabIndex = 55;
            this.lblIdEquipment.Text = "label1";
            // 
            // txtInventoryNumberEquipment
            // 
            this.txtInventoryNumberEquipment.Location = new System.Drawing.Point(605, 97);
            this.txtInventoryNumberEquipment.Name = "txtInventoryNumberEquipment";
            this.txtInventoryNumberEquipment.Size = new System.Drawing.Size(100, 20);
            this.txtInventoryNumberEquipment.TabIndex = 54;
            // 
            // txtNameEquipment
            // 
            this.txtNameEquipment.Location = new System.Drawing.Point(605, 59);
            this.txtNameEquipment.Name = "txtNameEquipment";
            this.txtNameEquipment.Size = new System.Drawing.Size(100, 20);
            this.txtNameEquipment.TabIndex = 53;
            // 
            // deleteEquipment
            // 
            this.deleteEquipment.Location = new System.Drawing.Point(725, 134);
            this.deleteEquipment.Name = "deleteEquipment";
            this.deleteEquipment.Size = new System.Drawing.Size(75, 23);
            this.deleteEquipment.TabIndex = 52;
            this.deleteEquipment.Text = "Удалить";
            this.deleteEquipment.UseVisualStyleBackColor = true;
            this.deleteEquipment.Click += new System.EventHandler(this.deleteEquipment_Click);
            // 
            // updateEquipment
            // 
            this.updateEquipment.Location = new System.Drawing.Point(725, 95);
            this.updateEquipment.Name = "updateEquipment";
            this.updateEquipment.Size = new System.Drawing.Size(75, 23);
            this.updateEquipment.TabIndex = 51;
            this.updateEquipment.Text = "Изменить";
            this.updateEquipment.UseVisualStyleBackColor = true;
            this.updateEquipment.Click += new System.EventHandler(this.updateEquipment_Click);
            // 
            // addEquipment
            // 
            this.addEquipment.Location = new System.Drawing.Point(725, 56);
            this.addEquipment.Name = "addEquipment";
            this.addEquipment.Size = new System.Drawing.Size(75, 23);
            this.addEquipment.TabIndex = 50;
            this.addEquipment.Text = "Добавить";
            this.addEquipment.UseVisualStyleBackColor = true;
            this.addEquipment.Click += new System.EventHandler(this.addEquipment_Click);
            // 
            // grdEquipments
            // 
            this.grdEquipments.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdEquipments.FullRowSelect = true;
            this.grdEquipments.GridLines = true;
            this.grdEquipments.Location = new System.Drawing.Point(8, 59);
            this.grdEquipments.Name = "grdEquipments";
            this.grdEquipments.Size = new System.Drawing.Size(489, 262);
            this.grdEquipments.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdEquipments.TabIndex = 49;
            this.grdEquipments.UseCompatibleStateImageBehavior = false;
            this.grdEquipments.View = System.Windows.Forms.View.Details;
            this.grdEquipments.SelectedIndexChanged += new System.EventHandler(this.grdEquipments_SelectedIndexChanged_1);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label38);
            this.tabPage3.Controls.Add(this.txtEdIzmMaterial);
            this.tabPage3.Controls.Add(this.cmbBoxEmployeersForMaterials);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.txtFilterMaterials);
            this.tabPage3.Controls.Add(this.lblIdMaterial);
            this.tabPage3.Controls.Add(this.txtQuantityMaterial);
            this.tabPage3.Controls.Add(this.txtNameMaterial);
            this.tabPage3.Controls.Add(this.deleteMaterial);
            this.tabPage3.Controls.Add(this.updateMaterial);
            this.tabPage3.Controls.Add(this.addMaterial);
            this.tabPage3.Controls.Add(this.grdMaterials);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1023, 329);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Работа с базой материалов";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Enter += new System.EventHandler(this.tabPage3_Enter);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(515, 137);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(52, 13);
            this.label38.TabIndex = 78;
            this.label38.Text = "Ед. изм.:";
            // 
            // txtEdIzmMaterial
            // 
            this.txtEdIzmMaterial.Location = new System.Drawing.Point(605, 134);
            this.txtEdIzmMaterial.Name = "txtEdIzmMaterial";
            this.txtEdIzmMaterial.Size = new System.Drawing.Size(100, 20);
            this.txtEdIzmMaterial.TabIndex = 77;
            // 
            // cmbBoxEmployeersForMaterials
            // 
            this.cmbBoxEmployeersForMaterials.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbBoxEmployeersForMaterials.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBoxEmployeersForMaterials.FormattingEnabled = true;
            this.cmbBoxEmployeersForMaterials.Location = new System.Drawing.Point(605, 177);
            this.cmbBoxEmployeersForMaterials.Name = "cmbBoxEmployeersForMaterials";
            this.cmbBoxEmployeersForMaterials.Size = new System.Drawing.Size(263, 21);
            this.cmbBoxEmployeersForMaterials.TabIndex = 76;
            this.cmbBoxEmployeersForMaterials.SelectedIndexChanged += new System.EventHandler(this.cmbBoxEmployeersForMaterials_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(515, 181);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 75;
            this.label11.Text = "Сотрудник:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(515, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 74;
            this.label12.Text = "Количество:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(515, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 13);
            this.label13.TabIndex = 73;
            this.label13.Text = "Наименование:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(259, 13);
            this.label14.TabIndex = 72;
            this.label14.Text = "Введите часть названия материала для фильтра:";
            // 
            // txtFilterMaterials
            // 
            this.txtFilterMaterials.Location = new System.Drawing.Point(276, 11);
            this.txtFilterMaterials.Name = "txtFilterMaterials";
            this.txtFilterMaterials.Size = new System.Drawing.Size(149, 20);
            this.txtFilterMaterials.TabIndex = 71;
            this.txtFilterMaterials.TextChanged += new System.EventHandler(this.txtFilterMaterials_TextChanged);
            // 
            // lblIdMaterial
            // 
            this.lblIdMaterial.AutoSize = true;
            this.lblIdMaterial.Location = new System.Drawing.Point(833, 8);
            this.lblIdMaterial.Name = "lblIdMaterial";
            this.lblIdMaterial.Size = new System.Drawing.Size(35, 13);
            this.lblIdMaterial.TabIndex = 70;
            this.lblIdMaterial.Text = "label1";
            // 
            // txtQuantityMaterial
            // 
            this.txtQuantityMaterial.Location = new System.Drawing.Point(605, 97);
            this.txtQuantityMaterial.Name = "txtQuantityMaterial";
            this.txtQuantityMaterial.Size = new System.Drawing.Size(100, 20);
            this.txtQuantityMaterial.TabIndex = 69;
            // 
            // txtNameMaterial
            // 
            this.txtNameMaterial.Location = new System.Drawing.Point(605, 59);
            this.txtNameMaterial.Name = "txtNameMaterial";
            this.txtNameMaterial.Size = new System.Drawing.Size(100, 20);
            this.txtNameMaterial.TabIndex = 68;
            // 
            // deleteMaterial
            // 
            this.deleteMaterial.Location = new System.Drawing.Point(725, 134);
            this.deleteMaterial.Name = "deleteMaterial";
            this.deleteMaterial.Size = new System.Drawing.Size(75, 23);
            this.deleteMaterial.TabIndex = 67;
            this.deleteMaterial.Text = "Удалить";
            this.deleteMaterial.UseVisualStyleBackColor = true;
            this.deleteMaterial.Click += new System.EventHandler(this.deleteMaterial_Click);
            // 
            // updateMaterial
            // 
            this.updateMaterial.Location = new System.Drawing.Point(725, 95);
            this.updateMaterial.Name = "updateMaterial";
            this.updateMaterial.Size = new System.Drawing.Size(75, 23);
            this.updateMaterial.TabIndex = 66;
            this.updateMaterial.Text = "Изменить";
            this.updateMaterial.UseVisualStyleBackColor = true;
            this.updateMaterial.Click += new System.EventHandler(this.updateMaterial_Click);
            // 
            // addMaterial
            // 
            this.addMaterial.Location = new System.Drawing.Point(725, 56);
            this.addMaterial.Name = "addMaterial";
            this.addMaterial.Size = new System.Drawing.Size(75, 23);
            this.addMaterial.TabIndex = 65;
            this.addMaterial.Text = "Добавить";
            this.addMaterial.UseVisualStyleBackColor = true;
            this.addMaterial.Click += new System.EventHandler(this.addMaterial_Click);
            // 
            // grdMaterials
            // 
            this.grdMaterials.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdMaterials.FullRowSelect = true;
            this.grdMaterials.GridLines = true;
            this.grdMaterials.Location = new System.Drawing.Point(8, 59);
            this.grdMaterials.Name = "grdMaterials";
            this.grdMaterials.Size = new System.Drawing.Size(489, 262);
            this.grdMaterials.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdMaterials.TabIndex = 64;
            this.grdMaterials.UseCompatibleStateImageBehavior = false;
            this.grdMaterials.View = System.Windows.Forms.View.Details;
            this.grdMaterials.SelectedIndexChanged += new System.EventHandler(this.grdMaterials_SelectedIndexChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label39);
            this.tabPage4.Controls.Add(this.txtEdIzmPart);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.txtPartNumber);
            this.tabPage4.Controls.Add(this.cmbBoxEmployeersForParts);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.txtFilterParts);
            this.tabPage4.Controls.Add(this.lblIdPart);
            this.tabPage4.Controls.Add(this.txtQuantityPart);
            this.tabPage4.Controls.Add(this.txtNamePart);
            this.tabPage4.Controls.Add(this.deletePart);
            this.tabPage4.Controls.Add(this.updatePart);
            this.tabPage4.Controls.Add(this.addPart);
            this.tabPage4.Controls.Add(this.grdParts);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1023, 329);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Работа с базой запасных частей";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Enter += new System.EventHandler(this.tabPage4_Enter);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(515, 172);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(69, 13);
            this.label39.TabIndex = 93;
            this.label39.Text = "Количество:";
            // 
            // txtEdIzmPart
            // 
            this.txtEdIzmPart.Location = new System.Drawing.Point(605, 134);
            this.txtEdIzmPart.Name = "txtEdIzmPart";
            this.txtEdIzmPart.Size = new System.Drawing.Size(100, 20);
            this.txtEdIzmPart.TabIndex = 92;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(515, 99);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 13);
            this.label20.TabIndex = 91;
            this.label20.Text = "Парт-номер:";
            // 
            // txtPartNumber
            // 
            this.txtPartNumber.Location = new System.Drawing.Point(605, 96);
            this.txtPartNumber.Name = "txtPartNumber";
            this.txtPartNumber.Size = new System.Drawing.Size(100, 20);
            this.txtPartNumber.TabIndex = 90;
            // 
            // cmbBoxEmployeersForParts
            // 
            this.cmbBoxEmployeersForParts.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbBoxEmployeersForParts.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBoxEmployeersForParts.FormattingEnabled = true;
            this.cmbBoxEmployeersForParts.Location = new System.Drawing.Point(605, 215);
            this.cmbBoxEmployeersForParts.Name = "cmbBoxEmployeersForParts";
            this.cmbBoxEmployeersForParts.Size = new System.Drawing.Size(263, 21);
            this.cmbBoxEmployeersForParts.TabIndex = 89;
            this.cmbBoxEmployeersForParts.SelectedIndexChanged += new System.EventHandler(this.cmbBoxEmployeersForParts_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(515, 219);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 13);
            this.label15.TabIndex = 88;
            this.label15.Text = "Сотрудник:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(515, 137);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 87;
            this.label16.Text = "Ед. изм.:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(515, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 13);
            this.label17.TabIndex = 86;
            this.label17.Text = "Наименование:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(259, 13);
            this.label18.TabIndex = 85;
            this.label18.Text = "Введите часть названия материала для фильтра:";
            // 
            // txtFilterParts
            // 
            this.txtFilterParts.Location = new System.Drawing.Point(276, 11);
            this.txtFilterParts.Name = "txtFilterParts";
            this.txtFilterParts.Size = new System.Drawing.Size(149, 20);
            this.txtFilterParts.TabIndex = 84;
            this.txtFilterParts.TextChanged += new System.EventHandler(this.txtFilterParts_TextChanged);
            // 
            // lblIdPart
            // 
            this.lblIdPart.AutoSize = true;
            this.lblIdPart.Location = new System.Drawing.Point(833, 8);
            this.lblIdPart.Name = "lblIdPart";
            this.lblIdPart.Size = new System.Drawing.Size(35, 13);
            this.lblIdPart.TabIndex = 83;
            this.lblIdPart.Text = "label1";
            // 
            // txtQuantityPart
            // 
            this.txtQuantityPart.Location = new System.Drawing.Point(605, 169);
            this.txtQuantityPart.Name = "txtQuantityPart";
            this.txtQuantityPart.Size = new System.Drawing.Size(100, 20);
            this.txtQuantityPart.TabIndex = 82;
            // 
            // txtNamePart
            // 
            this.txtNamePart.Location = new System.Drawing.Point(605, 59);
            this.txtNamePart.Name = "txtNamePart";
            this.txtNamePart.Size = new System.Drawing.Size(100, 20);
            this.txtNamePart.TabIndex = 81;
            // 
            // deletePart
            // 
            this.deletePart.Location = new System.Drawing.Point(725, 134);
            this.deletePart.Name = "deletePart";
            this.deletePart.Size = new System.Drawing.Size(75, 23);
            this.deletePart.TabIndex = 80;
            this.deletePart.Text = "Удалить";
            this.deletePart.UseVisualStyleBackColor = true;
            this.deletePart.Click += new System.EventHandler(this.deletePart_Click);
            // 
            // updatePart
            // 
            this.updatePart.Location = new System.Drawing.Point(725, 95);
            this.updatePart.Name = "updatePart";
            this.updatePart.Size = new System.Drawing.Size(75, 23);
            this.updatePart.TabIndex = 79;
            this.updatePart.Text = "Изменить";
            this.updatePart.UseVisualStyleBackColor = true;
            this.updatePart.Click += new System.EventHandler(this.updatePart_Click);
            // 
            // addPart
            // 
            this.addPart.Location = new System.Drawing.Point(725, 56);
            this.addPart.Name = "addPart";
            this.addPart.Size = new System.Drawing.Size(75, 23);
            this.addPart.TabIndex = 78;
            this.addPart.Text = "Добавить";
            this.addPart.UseVisualStyleBackColor = true;
            this.addPart.Click += new System.EventHandler(this.addPart_Click);
            // 
            // grdParts
            // 
            this.grdParts.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdParts.FullRowSelect = true;
            this.grdParts.GridLines = true;
            this.grdParts.Location = new System.Drawing.Point(8, 59);
            this.grdParts.Name = "grdParts";
            this.grdParts.Size = new System.Drawing.Size(489, 262);
            this.grdParts.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdParts.TabIndex = 77;
            this.grdParts.UseCompatibleStateImageBehavior = false;
            this.grdParts.View = System.Windows.Forms.View.Details;
            this.grdParts.SelectedIndexChanged += new System.EventHandler(this.grdParts_SelectedIndexChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button5);
            this.tabPage5.Controls.Add(this.txtCommentsForHardwareRepair);
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.button2);
            this.tabPage5.Controls.Add(this.lblCountPartForRepair);
            this.tabPage5.Controls.Add(this.txtCountPartForRepair);
            this.tabPage5.Controls.Add(this.listAddPartForRepair);
            this.tabPage5.Controls.Add(this.listAddMaterialForRepair);
            this.tabPage5.Controls.Add(this.dateTimePicker1);
            this.tabPage5.Controls.Add(this.label25);
            this.tabPage5.Controls.Add(this.lblCountMaterialForRepair);
            this.tabPage5.Controls.Add(this.txtCountMaterialForRepair);
            this.tabPage5.Controls.Add(this.btnSaveHardwareRepair);
            this.tabPage5.Controls.Add(this.addPartForRepair);
            this.tabPage5.Controls.Add(this.groupBox1);
            this.tabPage5.Controls.Add(this.addMaterialForRepair);
            this.tabPage5.Controls.Add(this.label23);
            this.tabPage5.Controls.Add(this.lblIdPartRepair);
            this.tabPage5.Controls.Add(this.txtFilterPartForRepair);
            this.tabPage5.Controls.Add(this.grdPartsForRepair);
            this.tabPage5.Controls.Add(this.lblIdMaterialRepair);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Controls.Add(this.txtFilterMaterialForRepair);
            this.tabPage5.Controls.Add(this.grdMaterialsForRepair);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1023, 329);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Ремонт собственными силами";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Enter += new System.EventHandler(this.tabPage5_Enter);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(3, 281);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(196, 23);
            this.button5.TabIndex = 109;
            this.button5.Text = "Вернутся к выбору оборудования";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtCommentsForHardwareRepair
            // 
            this.txtCommentsForHardwareRepair.Location = new System.Drawing.Point(730, 37);
            this.txtCommentsForHardwareRepair.Multiline = true;
            this.txtCommentsForHardwareRepair.Name = "txtCommentsForHardwareRepair";
            this.txtCommentsForHardwareRepair.Size = new System.Drawing.Size(281, 53);
            this.txtCommentsForHardwareRepair.TabIndex = 108;
            this.txtCommentsForHardwareRepair.Text = "Комментарии";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(952, 194);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(59, 81);
            this.button4.TabIndex = 107;
            this.button4.Text = "Удалить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(952, 96);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(59, 95);
            this.button3.TabIndex = 106;
            this.button3.Text = "Удалить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(867, 281);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 23);
            this.button2.TabIndex = 105;
            this.button2.Text = "Печать АКТа";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblCountPartForRepair
            // 
            this.lblCountPartForRepair.AutoSize = true;
            this.lblCountPartForRepair.Location = new System.Drawing.Point(576, 35);
            this.lblCountPartForRepair.Name = "lblCountPartForRepair";
            this.lblCountPartForRepair.Size = new System.Drawing.Size(41, 13);
            this.lblCountPartForRepair.TabIndex = 104;
            this.lblCountPartForRepair.Text = "label25";
            // 
            // txtCountPartForRepair
            // 
            this.txtCountPartForRepair.Location = new System.Drawing.Point(414, 285);
            this.txtCountPartForRepair.Name = "txtCountPartForRepair";
            this.txtCountPartForRepair.Size = new System.Drawing.Size(44, 20);
            this.txtCountPartForRepair.TabIndex = 103;
            // 
            // listAddPartForRepair
            // 
            this.listAddPartForRepair.FormattingEnabled = true;
            this.listAddPartForRepair.Location = new System.Drawing.Point(730, 194);
            this.listAddPartForRepair.Name = "listAddPartForRepair";
            this.listAddPartForRepair.Size = new System.Drawing.Size(222, 82);
            this.listAddPartForRepair.TabIndex = 102;
            // 
            // listAddMaterialForRepair
            // 
            this.listAddMaterialForRepair.FormattingEnabled = true;
            this.listAddMaterialForRepair.Location = new System.Drawing.Point(730, 96);
            this.listAddMaterialForRepair.Name = "listAddMaterialForRepair";
            this.listAddMaterialForRepair.Size = new System.Drawing.Size(222, 95);
            this.listAddMaterialForRepair.TabIndex = 101;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(795, 11);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 20);
            this.dateTimePicker1.TabIndex = 100;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(727, 14);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(62, 13);
            this.label25.TabIndex = 99;
            this.label25.Text = "Дата акта:";
            // 
            // lblCountMaterialForRepair
            // 
            this.lblCountMaterialForRepair.AutoSize = true;
            this.lblCountMaterialForRepair.Location = new System.Drawing.Point(301, 34);
            this.lblCountMaterialForRepair.Name = "lblCountMaterialForRepair";
            this.lblCountMaterialForRepair.Size = new System.Drawing.Size(41, 13);
            this.lblCountMaterialForRepair.TabIndex = 98;
            this.lblCountMaterialForRepair.Text = "label25";
            // 
            // txtCountMaterialForRepair
            // 
            this.txtCountMaterialForRepair.Location = new System.Drawing.Point(214, 284);
            this.txtCountMaterialForRepair.Name = "txtCountMaterialForRepair";
            this.txtCountMaterialForRepair.Size = new System.Drawing.Size(44, 20);
            this.txtCountMaterialForRepair.TabIndex = 97;
            // 
            // btnSaveHardwareRepair
            // 
            this.btnSaveHardwareRepair.Location = new System.Drawing.Point(730, 282);
            this.btnSaveHardwareRepair.Name = "btnSaveHardwareRepair";
            this.btnSaveHardwareRepair.Size = new System.Drawing.Size(120, 23);
            this.btnSaveHardwareRepair.TabIndex = 95;
            this.btnSaveHardwareRepair.Text = "Сохранить";
            this.btnSaveHardwareRepair.UseVisualStyleBackColor = true;
            this.btnSaveHardwareRepair.Click += new System.EventHandler(this.btnSaveHardwareRepair_Click);
            // 
            // addPartForRepair
            // 
            this.addPartForRepair.Location = new System.Drawing.Point(497, 282);
            this.addPartForRepair.Name = "addPartForRepair";
            this.addPartForRepair.Size = new System.Drawing.Size(120, 23);
            this.addPartForRepair.TabIndex = 94;
            this.addPartForRepair.Text = "Добавить в акт   =>";
            this.addPartForRepair.UseVisualStyleBackColor = true;
            this.addPartForRepair.Click += new System.EventHandler(this.addPartForRepair_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbInventaryNumberEquipment);
            this.groupBox1.Controls.Add(this.lbEmployeeEquipment);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.lbConditionEquipment);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.lbNameEquipment);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.lblIdEquipmentRepair);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Location = new System.Drawing.Point(3, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(196, 264);
            this.groupBox1.TabIndex = 93;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация об оборудовании";
            // 
            // lbInventaryNumberEquipment
            // 
            this.lbInventaryNumberEquipment.AutoSize = true;
            this.lbInventaryNumberEquipment.Location = new System.Drawing.Point(7, 117);
            this.lbInventaryNumberEquipment.Name = "lbInventaryNumberEquipment";
            this.lbInventaryNumberEquipment.Size = new System.Drawing.Size(10, 13);
            this.lbInventaryNumberEquipment.TabIndex = 96;
            this.lbInventaryNumberEquipment.Text = "-";
            // 
            // lbEmployeeEquipment
            // 
            this.lbEmployeeEquipment.AutoSize = true;
            this.lbEmployeeEquipment.Location = new System.Drawing.Point(7, 204);
            this.lbEmployeeEquipment.Name = "lbEmployeeEquipment";
            this.lbEmployeeEquipment.Size = new System.Drawing.Size(10, 13);
            this.lbEmployeeEquipment.TabIndex = 95;
            this.lbEmployeeEquipment.Text = "-";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 143);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 13);
            this.label26.TabIndex = 93;
            this.label26.Text = "Состояние:";
            // 
            // lbConditionEquipment
            // 
            this.lbConditionEquipment.AutoSize = true;
            this.lbConditionEquipment.Location = new System.Drawing.Point(7, 167);
            this.lbConditionEquipment.Name = "lbConditionEquipment";
            this.lbConditionEquipment.Size = new System.Drawing.Size(10, 13);
            this.lbConditionEquipment.TabIndex = 94;
            this.lbConditionEquipment.Text = "-";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 13);
            this.label24.TabIndex = 91;
            this.label24.Text = "Наименование:";
            // 
            // lbNameEquipment
            // 
            this.lbNameEquipment.AutoSize = true;
            this.lbNameEquipment.Location = new System.Drawing.Point(6, 60);
            this.lbNameEquipment.Name = "lbNameEquipment";
            this.lbNameEquipment.Size = new System.Drawing.Size(10, 13);
            this.lbNameEquipment.TabIndex = 92;
            this.lbNameEquipment.Text = "-";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 92);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 60;
            this.label19.Text = "Инв. номер:";
            // 
            // lblIdEquipmentRepair
            // 
            this.lblIdEquipmentRepair.AutoSize = true;
            this.lblIdEquipmentRepair.Location = new System.Drawing.Point(48, 16);
            this.lblIdEquipmentRepair.Name = "lblIdEquipmentRepair";
            this.lblIdEquipmentRepair.Size = new System.Drawing.Size(35, 13);
            this.lblIdEquipmentRepair.TabIndex = 61;
            this.lblIdEquipmentRepair.Text = "label1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 190);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 13);
            this.label22.TabIndex = 90;
            this.label22.Text = "Сотрудник:";
            // 
            // addMaterialForRepair
            // 
            this.addMaterialForRepair.Location = new System.Drawing.Point(269, 282);
            this.addMaterialForRepair.Name = "addMaterialForRepair";
            this.addMaterialForRepair.Size = new System.Drawing.Size(120, 23);
            this.addMaterialForRepair.TabIndex = 92;
            this.addMaterialForRepair.Text = "Добавить в акт   =>";
            this.addMaterialForRepair.UseVisualStyleBackColor = true;
            this.addMaterialForRepair.Click += new System.EventHandler(this.addMaterialForRepair_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(401, 14);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 81;
            this.label23.Text = "Запчасть:";
            // 
            // lblIdPartRepair
            // 
            this.lblIdPartRepair.AutoSize = true;
            this.lblIdPartRepair.Location = new System.Drawing.Point(403, 35);
            this.lblIdPartRepair.Name = "lblIdPartRepair";
            this.lblIdPartRepair.Size = new System.Drawing.Size(35, 13);
            this.lblIdPartRepair.TabIndex = 80;
            this.lblIdPartRepair.Text = "label1";
            // 
            // txtFilterPartForRepair
            // 
            this.txtFilterPartForRepair.Location = new System.Drawing.Point(470, 11);
            this.txtFilterPartForRepair.Name = "txtFilterPartForRepair";
            this.txtFilterPartForRepair.Size = new System.Drawing.Size(243, 20);
            this.txtFilterPartForRepair.TabIndex = 78;
            this.txtFilterPartForRepair.TextChanged += new System.EventHandler(this.txtFilterPartForRepair_TextChanged);
            // 
            // grdPartsForRepair
            // 
            this.grdPartsForRepair.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdPartsForRepair.FullRowSelect = true;
            this.grdPartsForRepair.GridLines = true;
            this.grdPartsForRepair.Location = new System.Drawing.Point(403, 57);
            this.grdPartsForRepair.Name = "grdPartsForRepair";
            this.grdPartsForRepair.Size = new System.Drawing.Size(310, 219);
            this.grdPartsForRepair.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdPartsForRepair.TabIndex = 77;
            this.grdPartsForRepair.UseCompatibleStateImageBehavior = false;
            this.grdPartsForRepair.View = System.Windows.Forms.View.Details;
            this.grdPartsForRepair.SelectedIndexChanged += new System.EventHandler(this.grdPartsForRepair_SelectedIndexChanged);
            // 
            // lblIdMaterialRepair
            // 
            this.lblIdMaterialRepair.AutoSize = true;
            this.lblIdMaterialRepair.Location = new System.Drawing.Point(208, 35);
            this.lblIdMaterialRepair.Name = "lblIdMaterialRepair";
            this.lblIdMaterialRepair.Size = new System.Drawing.Size(35, 13);
            this.lblIdMaterialRepair.TabIndex = 76;
            this.lblIdMaterialRepair.Text = "label1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(208, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 75;
            this.label21.Text = "Материал:";
            // 
            // txtFilterMaterialForRepair
            // 
            this.txtFilterMaterialForRepair.Location = new System.Drawing.Point(275, 11);
            this.txtFilterMaterialForRepair.Name = "txtFilterMaterialForRepair";
            this.txtFilterMaterialForRepair.Size = new System.Drawing.Size(120, 20);
            this.txtFilterMaterialForRepair.TabIndex = 74;
            this.txtFilterMaterialForRepair.TextChanged += new System.EventHandler(this.txtFilterMaterialForRepair_TextChanged);
            // 
            // grdMaterialsForRepair
            // 
            this.grdMaterialsForRepair.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdMaterialsForRepair.FullRowSelect = true;
            this.grdMaterialsForRepair.GridLines = true;
            this.grdMaterialsForRepair.Location = new System.Drawing.Point(208, 57);
            this.grdMaterialsForRepair.Name = "grdMaterialsForRepair";
            this.grdMaterialsForRepair.Size = new System.Drawing.Size(187, 219);
            this.grdMaterialsForRepair.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdMaterialsForRepair.TabIndex = 73;
            this.grdMaterialsForRepair.UseCompatibleStateImageBehavior = false;
            this.grdMaterialsForRepair.View = System.Windows.Forms.View.Details;
            this.grdMaterialsForRepair.SelectedIndexChanged += new System.EventHandler(this.grdMaterialsForRepair_SelectedIndexChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.btnSaveAccountingRepair_Click);
            this.tabPage6.Controls.Add(this.lblIdOrganizationForRepair);
            this.tabPage6.Controls.Add(this.label37);
            this.tabPage6.Controls.Add(this.txtPhoneOrganizationForRepair);
            this.tabPage6.Controls.Add(this.label34);
            this.tabPage6.Controls.Add(this.label36);
            this.tabPage6.Controls.Add(this.txtAdresOrganizationForRepair);
            this.tabPage6.Controls.Add(this.txtNameOrganizationForRepair);
            this.tabPage6.Controls.Add(this.deleteOrganization);
            this.tabPage6.Controls.Add(this.updateOrganization);
            this.tabPage6.Controls.Add(this.addOrganization);
            this.tabPage6.Controls.Add(this.label32);
            this.tabPage6.Controls.Add(this.txtFilterOrganizationForRepair);
            this.tabPage6.Controls.Add(this.grdOrganizationForRepair);
            this.tabPage6.Controls.Add(this.groupBox4);
            this.tabPage6.Controls.Add(this.button11);
            this.tabPage6.Controls.Add(this.groupBox2);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1023, 329);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Ремонты сторонними организациями";
            this.tabPage6.UseVisualStyleBackColor = true;
            this.tabPage6.Enter += new System.EventHandler(this.tabPage6_Enter);
            // 
            // btnSaveAccountingRepair_Click
            // 
            this.btnSaveAccountingRepair_Click.Location = new System.Drawing.Point(822, 200);
            this.btnSaveAccountingRepair_Click.Name = "btnSaveAccountingRepair_Click";
            this.btnSaveAccountingRepair_Click.Size = new System.Drawing.Size(120, 23);
            this.btnSaveAccountingRepair_Click.TabIndex = 127;
            this.btnSaveAccountingRepair_Click.Text = "Сохранить";
            this.btnSaveAccountingRepair_Click.UseVisualStyleBackColor = true;
            this.btnSaveAccountingRepair_Click.Click += new System.EventHandler(this.btnSaveAccountingRepair_Click_Click);
            // 
            // lblIdOrganizationForRepair
            // 
            this.lblIdOrganizationForRepair.AutoSize = true;
            this.lblIdOrganizationForRepair.Location = new System.Drawing.Point(386, 50);
            this.lblIdOrganizationForRepair.Name = "lblIdOrganizationForRepair";
            this.lblIdOrganizationForRepair.Size = new System.Drawing.Size(41, 13);
            this.lblIdOrganizationForRepair.TabIndex = 126;
            this.lblIdOrganizationForRepair.Text = "label38";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(480, 108);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(55, 13);
            this.label37.TabIndex = 125;
            this.label37.Text = "Телефон:";
            // 
            // txtPhoneOrganizationForRepair
            // 
            this.txtPhoneOrganizationForRepair.Location = new System.Drawing.Point(482, 124);
            this.txtPhoneOrganizationForRepair.Name = "txtPhoneOrganizationForRepair";
            this.txtPhoneOrganizationForRepair.Size = new System.Drawing.Size(263, 20);
            this.txtPhoneOrganizationForRepair.TabIndex = 124;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(479, 64);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(47, 13);
            this.label34.TabIndex = 123;
            this.label34.Text = "Адресс:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(478, 14);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(86, 13);
            this.label36.TabIndex = 122;
            this.label36.Text = "Наименование:";
            // 
            // txtAdresOrganizationForRepair
            // 
            this.txtAdresOrganizationForRepair.Location = new System.Drawing.Point(481, 80);
            this.txtAdresOrganizationForRepair.Name = "txtAdresOrganizationForRepair";
            this.txtAdresOrganizationForRepair.Size = new System.Drawing.Size(263, 20);
            this.txtAdresOrganizationForRepair.TabIndex = 121;
            // 
            // txtNameOrganizationForRepair
            // 
            this.txtNameOrganizationForRepair.Location = new System.Drawing.Point(482, 33);
            this.txtNameOrganizationForRepair.Name = "txtNameOrganizationForRepair";
            this.txtNameOrganizationForRepair.Size = new System.Drawing.Size(262, 20);
            this.txtNameOrganizationForRepair.TabIndex = 120;
            // 
            // deleteOrganization
            // 
            this.deleteOrganization.Location = new System.Drawing.Point(670, 157);
            this.deleteOrganization.Name = "deleteOrganization";
            this.deleteOrganization.Size = new System.Drawing.Size(75, 23);
            this.deleteOrganization.TabIndex = 119;
            this.deleteOrganization.Text = "Удалить";
            this.deleteOrganization.UseVisualStyleBackColor = true;
            this.deleteOrganization.Click += new System.EventHandler(this.deleteOrganization_Click);
            // 
            // updateOrganization
            // 
            this.updateOrganization.Location = new System.Drawing.Point(577, 157);
            this.updateOrganization.Name = "updateOrganization";
            this.updateOrganization.Size = new System.Drawing.Size(75, 23);
            this.updateOrganization.TabIndex = 118;
            this.updateOrganization.Text = "Изменить";
            this.updateOrganization.UseVisualStyleBackColor = true;
            this.updateOrganization.Click += new System.EventHandler(this.updateOrganization_Click);
            // 
            // addOrganization
            // 
            this.addOrganization.Location = new System.Drawing.Point(482, 157);
            this.addOrganization.Name = "addOrganization";
            this.addOrganization.Size = new System.Drawing.Size(75, 23);
            this.addOrganization.TabIndex = 117;
            this.addOrganization.Text = "Добавить";
            this.addOrganization.UseVisualStyleBackColor = true;
            this.addOrganization.Click += new System.EventHandler(this.addOrganization_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(169, 14);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(269, 13);
            this.label32.TabIndex = 116;
            this.label32.Text = "Введите часть названия организации для фильтра:";
            // 
            // txtFilterOrganizationForRepair
            // 
            this.txtFilterOrganizationForRepair.Location = new System.Drawing.Point(169, 33);
            this.txtFilterOrganizationForRepair.Name = "txtFilterOrganizationForRepair";
            this.txtFilterOrganizationForRepair.Size = new System.Drawing.Size(307, 20);
            this.txtFilterOrganizationForRepair.TabIndex = 115;
            this.txtFilterOrganizationForRepair.TextChanged += new System.EventHandler(this.txtFilterOrganizationForRepair_TextChanged);
            // 
            // grdOrganizationForRepair
            // 
            this.grdOrganizationForRepair.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdOrganizationForRepair.FullRowSelect = true;
            this.grdOrganizationForRepair.GridLines = true;
            this.grdOrganizationForRepair.Location = new System.Drawing.Point(169, 64);
            this.grdOrganizationForRepair.Name = "grdOrganizationForRepair";
            this.grdOrganizationForRepair.Size = new System.Drawing.Size(307, 214);
            this.grdOrganizationForRepair.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdOrganizationForRepair.TabIndex = 114;
            this.grdOrganizationForRepair.UseCompatibleStateImageBehavior = false;
            this.grdOrganizationForRepair.View = System.Windows.Forms.View.Details;
            this.grdOrganizationForRepair.SelectedIndexChanged += new System.EventHandler(this.grdOrganizationForRepair_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtCostWhatWork);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.txtWhatWork);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.dateAccountingRepair);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Location = new System.Drawing.Point(750, 15);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(242, 179);
            this.groupBox4.TabIndex = 113;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Описания ремонта с указанием стоимости";
            // 
            // txtCostWhatWork
            // 
            this.txtCostWhatWork.Location = new System.Drawing.Point(72, 153);
            this.txtCostWhatWork.Name = "txtCostWhatWork";
            this.txtCostWhatWork.Size = new System.Drawing.Size(100, 20);
            this.txtCostWhatWork.TabIndex = 5;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 156);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(65, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "Стоимость:";
            // 
            // txtWhatWork
            // 
            this.txtWhatWork.Location = new System.Drawing.Point(8, 56);
            this.txtWhatWork.Multiline = true;
            this.txtWhatWork.Name = "txtWhatWork";
            this.txtWhatWork.Size = new System.Drawing.Size(227, 88);
            this.txtWhatWork.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(60, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "Описание:";
            // 
            // dateAccountingRepair
            // 
            this.dateAccountingRepair.Location = new System.Drawing.Point(45, 19);
            this.dateAccountingRepair.Name = "dateAccountingRepair";
            this.dateAccountingRepair.Size = new System.Drawing.Size(124, 20);
            this.dateAccountingRepair.TabIndex = 1;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 21);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(36, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Дата:";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(3, 281);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(196, 23);
            this.button11.TabIndex = 111;
            this.button11.Text = "Вернутся к выбору оборудования";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbInventaryNumberEquipmentAccount);
            this.groupBox2.Controls.Add(this.lbEmployeeEquipmentAccount);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.lbConditionEquipmentAccount);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.lbNameEquipmentAccount);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.lblIdEquipmentRepairAccount);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Location = new System.Drawing.Point(3, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(162, 264);
            this.groupBox2.TabIndex = 110;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Информация об оборудовании";
            // 
            // lbInventaryNumberEquipmentAccount
            // 
            this.lbInventaryNumberEquipmentAccount.AutoSize = true;
            this.lbInventaryNumberEquipmentAccount.Location = new System.Drawing.Point(7, 117);
            this.lbInventaryNumberEquipmentAccount.Name = "lbInventaryNumberEquipmentAccount";
            this.lbInventaryNumberEquipmentAccount.Size = new System.Drawing.Size(10, 13);
            this.lbInventaryNumberEquipmentAccount.TabIndex = 96;
            this.lbInventaryNumberEquipmentAccount.Text = "-";
            // 
            // lbEmployeeEquipmentAccount
            // 
            this.lbEmployeeEquipmentAccount.AutoSize = true;
            this.lbEmployeeEquipmentAccount.Location = new System.Drawing.Point(7, 204);
            this.lbEmployeeEquipmentAccount.Name = "lbEmployeeEquipmentAccount";
            this.lbEmployeeEquipmentAccount.Size = new System.Drawing.Size(10, 13);
            this.lbEmployeeEquipmentAccount.TabIndex = 95;
            this.lbEmployeeEquipmentAccount.Text = "-";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 143);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 93;
            this.label29.Text = "Состояние:";
            // 
            // lbConditionEquipmentAccount
            // 
            this.lbConditionEquipmentAccount.AutoSize = true;
            this.lbConditionEquipmentAccount.Location = new System.Drawing.Point(7, 167);
            this.lbConditionEquipmentAccount.Name = "lbConditionEquipmentAccount";
            this.lbConditionEquipmentAccount.Size = new System.Drawing.Size(10, 13);
            this.lbConditionEquipmentAccount.TabIndex = 94;
            this.lbConditionEquipmentAccount.Text = "-";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 38);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(86, 13);
            this.label31.TabIndex = 91;
            this.label31.Text = "Наименование:";
            // 
            // lbNameEquipmentAccount
            // 
            this.lbNameEquipmentAccount.AutoSize = true;
            this.lbNameEquipmentAccount.Location = new System.Drawing.Point(6, 60);
            this.lbNameEquipmentAccount.Name = "lbNameEquipmentAccount";
            this.lbNameEquipmentAccount.Size = new System.Drawing.Size(10, 13);
            this.lbNameEquipmentAccount.TabIndex = 92;
            this.lbNameEquipmentAccount.Text = "-";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 92);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 13);
            this.label33.TabIndex = 60;
            this.label33.Text = "Инв. номер:";
            // 
            // lblIdEquipmentRepairAccount
            // 
            this.lblIdEquipmentRepairAccount.AutoSize = true;
            this.lblIdEquipmentRepairAccount.Location = new System.Drawing.Point(48, 16);
            this.lblIdEquipmentRepairAccount.Name = "lblIdEquipmentRepairAccount";
            this.lblIdEquipmentRepairAccount.Size = new System.Drawing.Size(35, 13);
            this.lblIdEquipmentRepairAccount.TabIndex = 61;
            this.lblIdEquipmentRepairAccount.Text = "label1";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 190);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(63, 13);
            this.label35.TabIndex = 90;
            this.label35.Text = "Сотрудник:";
            // 
            // reportViewer1
            // 
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(396, 246);
            this.reportViewer1.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 394);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Автоматизированная система учета компьютеров, расхлдных и комплектующих материало" +
    "в";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem работаСБДToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сотрудникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оборудованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem материалыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ремонтыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem собственнымиСиламиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem стороннимиОрганизациямиToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private ListView grdEmployees;
        private Button deleteEmployee;
        private Button updateEmployee;
        private Button addEmployee;
        private TextBox txtPatronymic;
        private TextBox txtSoname;
        private TextBox txtName;
        private Label lblIdEmployee;
        private Label label1;
        private TextBox filterEmployee;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label5;
        private TextBox txtTabNom;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private TextBox txtFilterEquipments;
        private Label lblIdEquipment;
        private TextBox txtInventoryNumberEquipment;
        private TextBox txtNameEquipment;
        private Button deleteEquipment;
        private Button updateEquipment;
        private Button addEquipment;
        private ListView grdEquipments;
        private ComboBox cBoxCondition;
        private Label label10;
        private ComboBox cmbBoxEmployeersForEquipments;
        private ToolStripMenuItem запасныеЧастиToolStripMenuItem;
        private TabPage tabPage6;
        private ComboBox cmbBoxEmployeersForMaterials;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private TextBox txtFilterMaterials;
        private Label lblIdMaterial;
        private TextBox txtQuantityMaterial;
        private TextBox txtNameMaterial;
        private Button deleteMaterial;
        private Button updateMaterial;
        private Button addMaterial;
        private ListView grdMaterials;
        private Label label20;
        private TextBox txtPartNumber;
        private ComboBox cmbBoxEmployeersForParts;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private TextBox txtFilterParts;
        private Label lblIdPart;
        private TextBox txtQuantityPart;
        private TextBox txtNamePart;
        private Button deletePart;
        private Button updatePart;
        private Button addPart;
        private ListView grdParts;
        private Label lblIdEquipmentRepair;
        private Label label19;
        private Button addMaterialForRepair;
        private Label label22;
        private Label label23;
        private Label lblIdPartRepair;
        private TextBox txtFilterPartForRepair;
        private ListView grdPartsForRepair;
        private Label lblIdMaterialRepair;
        private Label label21;
        private TextBox txtFilterMaterialForRepair;
        private ListView grdMaterialsForRepair;
        private Button repairForeignOrganization;
        private Button repairOnItsOwn;
        private GroupBox groupBox1;
        private Label lbInventaryNumberEquipment;
        private Label lbEmployeeEquipment;
        private Label label26;
        private Label lbConditionEquipment;
        private Label label24;
        private Label lbNameEquipment;
        private Button addPartForRepair;
        private Button btnSaveHardwareRepair;
        private TextBox txtCountMaterialForRepair;
        private Label lblCountMaterialForRepair;
        private DateTimePicker dateTimePicker1;
        private Label label25;
        private ListBox listAddMaterialForRepair;
        private ListBox listAddPartForRepair;
        private TextBox txtCountPartForRepair;
        private Label lblCountPartForRepair;
        private Button button2;
        private Button button4;
        private Button button3;
        private TextBox txtCommentsForHardwareRepair;
        private Button button5;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private Button button6;
        private Button button7;
        private Button button9;
        private Button button8;
        private Button button10;
        private Button button11;
        private GroupBox groupBox2;
        private Label lbInventaryNumberEquipmentAccount;
        private Label lbEmployeeEquipmentAccount;
        private Label label29;
        private Label lbConditionEquipmentAccount;
        private Label label31;
        private Label lbNameEquipmentAccount;
        private Label label33;
        private Label lblIdEquipmentRepairAccount;
        private Label label35;
        private GroupBox groupBox4;
        private TextBox txtCostWhatWork;
        private Label label30;
        private TextBox txtWhatWork;
        private Label label28;
        private DateTimePicker dateAccountingRepair;
        private Label label27;
        private Label label32;
        private TextBox txtFilterOrganizationForRepair;
        private ListView grdOrganizationForRepair;
        private Label label34;
        private Label label36;
        private TextBox txtAdresOrganizationForRepair;
        private TextBox txtNameOrganizationForRepair;
        private Button deleteOrganization;
        private Button updateOrganization;
        private Button addOrganization;
        private Label label37;
        private TextBox txtPhoneOrganizationForRepair;
        private Label lblIdOrganizationForRepair;
        private Button btnSaveAccountingRepair_Click;
        private ToolStripMenuItem ремонтныеОрганизацииToolStripMenuItem;
        private ToolStripMenuItem просмотрпечатьИсторииРемонтовToolStripMenuItem;
        private Label label38;
        private TextBox txtEdIzmMaterial;
        private Label label39;
        private TextBox txtEdIzmPart;
    }
}

