﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IMaterialRepository
    {
        List<Material> Materials { get; }
        bool Delete(Material material);
        bool Add(Material material);
        bool Update(Material material);
    }
}
