﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class EmployeeRepository : IEmployeeRepository
    {
        private readonly Context _context;

        public EmployeeRepository(Context context)
        {
            _context = context;
        }
        public List<Employee> Employees
        {
            get
            {
                try
                {
                    List<Employee> employees = _context.Employees.ToList();
                    return employees;
                }
                catch
                {
                    return null;
                }
            }
        }
        public bool Delete(Employee employee)
        {
            try
            {
                _context.Employees.Remove(employee);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(Employee employee)
        {
            try
            {
                _context.Employees.Add(employee);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Employee employee)
        {
            try
            {
                _context.Employees.AddOrUpdate(employee);
                _context.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
    }
}
