﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IOrganizationRepository
    {
        List<Organization> Organizations { get; }
        bool Delete(Organization organization);
        bool Add(Organization organization);
        bool Update(Organization organization);
    }
}
