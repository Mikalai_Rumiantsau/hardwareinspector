﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class MaterialRepository : IMaterialRepository
    {
        private readonly Context _context;

        public MaterialRepository(Context context)
        {
            _context = context;
        }
        public List<Material> Materials
        {
            get
            {
                try
                {
                    List<Material> materials = _context.Materials.ToList();
                    return materials;
                }
                catch
                {
                    return null;
                }
            }
        }
        public bool Delete(Material material)
        {
            try
            {
                _context.Materials.Remove(material);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(Material material)
        {
            try
            {
                _context.Materials.Add(material);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Material material)
        {
            try
            {
                _context.Materials.AddOrUpdate(material);
                _context.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }
    }
}
