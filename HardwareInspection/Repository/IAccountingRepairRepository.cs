﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IAccountingRepairRepository
    {
        List<AccountingRepair> AccountingRepairs { get; }
        bool Delete(AccountingRepair accountingRepair);
        bool Add(AccountingRepair accountingRepair);
        bool Update(AccountingRepair accountingRepair);
    }
}
