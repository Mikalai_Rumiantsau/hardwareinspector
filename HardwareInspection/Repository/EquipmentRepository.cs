﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class EquipmentRepository : IEquipmentRepository
    {
        private readonly Context _context;

        public EquipmentRepository(Context context)
        {
            _context = context;
        }
        public List<Equipment> Equipments 
        {
            get
            {
                try
                {
                    List<Equipment> equipments = _context.Equipments.ToList();
                    return equipments;
                }
                catch
                {
                    return null;
                }
            }
        }

        public bool Delete(Equipment equipment)
        {
            try
            {
                _context.Equipments.Remove(equipment);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(Equipment equipment)
        {
            try
            {
                _context.Equipments.Add(equipment);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Equipment equipment)
        {
            try
            {
                _context.Equipments.AddOrUpdate(equipment);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
