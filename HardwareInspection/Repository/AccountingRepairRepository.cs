﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class AccountingRepairRepository : IAccountingRepairRepository
    {
        private readonly Context _context;

        public AccountingRepairRepository(Context context)
        {
            _context = context;
        }
        public List<AccountingRepair> AccountingRepairs
        {
            get
            {
                try
                {
                    List<AccountingRepair> accountingRepairs = _context.AccountingRepairs.ToList();
                    return accountingRepairs;
                }
                catch
                {
                    return null;
                }
            }
        }
        public bool Delete(AccountingRepair accountingRepair)
        {
            try
            {
                _context.AccountingRepairs.Remove(accountingRepair);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(AccountingRepair accountingRepair)
        {
            try
            {
                _context.AccountingRepairs.Add(accountingRepair);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(AccountingRepair accountingRepair)
        {
            try
            {
                _context.AccountingRepairs.AddOrUpdate(accountingRepair);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
