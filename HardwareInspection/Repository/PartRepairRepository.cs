﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class PartRepairRepository : IPartRepairRepository
    {
        private readonly Context _context;

        public PartRepairRepository(Context context)
        {
            _context = context;
        }
        public List<PartRepair> PartRepairs
        {
            get
            {
                try
                {
                    List<PartRepair> parts = _context.PartRepairs.ToList();
                    return parts;
                }
                catch
                {
                    return null;
                }
            }
        }
        public bool Delete(PartRepair part)
        {
            try
            {
                _context.PartRepairs.Remove(part);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(PartRepair part)
        {
            try
            {
                _context.PartRepairs.Add(part);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(PartRepair part)
        {
            try
            {
                _context.PartRepairs.AddOrUpdate(part);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
