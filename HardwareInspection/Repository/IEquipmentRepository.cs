﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IEquipmentRepository
    {
        List<Equipment> Equipments { get; }
        bool Delete(Equipment equipment);
        bool Add(Equipment equipment);
        bool Update(Equipment equipment);
    }
}
