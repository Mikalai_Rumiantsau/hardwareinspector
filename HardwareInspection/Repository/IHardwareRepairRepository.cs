﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IHardwareRepairRepository
    {
        List<HardwareRepair> HardwareRepairs { get; }
        bool Delete(HardwareRepair hardwareRepair);
        bool Add(HardwareRepair hardwareRepair);
        bool Update(HardwareRepair hardwareRepair);
    }
}
