﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class PartRepository : IPartRepository
    {
        private readonly Context _context;

        public PartRepository(Context context)
        {
            _context = context;
        }
        public List<Part> Parts
        {
            get
            {
                try
                {
                    List<Part> parts = _context.Parts.ToList();
                    return parts;
                }
                catch
                {
                    return null;
                }
            }
        }
        public bool Delete(Part part)
        {
            try
            {
                _context.Parts.Remove(part);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(Part part)
        {
            try
            {
                _context.Parts.Add(part);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Part part)
        {
            try
            {
                _context.Parts.AddOrUpdate(part);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
