﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class HardwareRepairRepository : IHardwareRepairRepository
    {
        private readonly Context _context;

        public HardwareRepairRepository(Context context)
        {
            _context = context;
        }
        public List<HardwareRepair> HardwareRepairs
        {
            get
            {
                try
                {
                    List<HardwareRepair> hardwareRepairs = _context.HardwareRepairs.ToList();
                    return hardwareRepairs;
                }
                catch
                {
                    return null;
                }
            }
        }
        public bool Delete(HardwareRepair hardwareRepair)
        {
            try
            {
                _context.HardwareRepairs.Remove(hardwareRepair);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(HardwareRepair hardwareRepair)
        {
            try
            {
                _context.HardwareRepairs.Add(hardwareRepair);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(HardwareRepair hardwareRepair)
        {
            try
            {
                _context.HardwareRepairs.AddOrUpdate(hardwareRepair);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
