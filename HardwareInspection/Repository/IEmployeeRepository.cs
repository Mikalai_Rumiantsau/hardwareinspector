﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IEmployeeRepository
    {
        List<Employee> Employees { get; }
        bool Delete(Employee employee);
        bool Add(Employee employee);
        bool Update(Employee employee);
    }
}
