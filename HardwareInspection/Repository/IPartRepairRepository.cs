﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IPartRepairRepository
    {
        List<PartRepair> PartRepairs { get; }
        bool Delete(PartRepair part);
        bool Add(PartRepair part);
        bool Update(PartRepair part);
    }
}
