﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class OrganizationRepository : IOrganizationRepository
    {
        private readonly Context _context;

        public OrganizationRepository(Context context)
        {
            _context = context;
        }

        public List<Organization> Organizations
        {
            get
            {
                try
                {
                    List<Organization> organizations = _context.Organizations.ToList();
                    return organizations;
                }
                catch
                {
                    return null;
                }
            }
        }
        
        public bool Delete(Organization organization)
        {
            try
            {
                _context.Organizations.Remove(organization);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(Organization organization)
        {
            try
            {
                _context.Organizations.Add(organization);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Organization organization)
        {
            try
            {
                _context.Organizations.AddOrUpdate(organization);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
