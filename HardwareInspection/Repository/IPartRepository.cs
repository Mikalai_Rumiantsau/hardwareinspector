﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IPartRepository
    {
        List<Part> Parts { get; }
        bool Delete(Part part);
        bool Add(Part part);
        bool Update(Part part);
    }
}
