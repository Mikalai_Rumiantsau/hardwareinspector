﻿using HardwareInspection.Model;
using System.Collections.Generic;

namespace HardwareInspection.Repository
{
    interface IMaterialRepairRepository
    {
        List<MaterialRepair> MaterialRepairs { get; }
        bool Delete(MaterialRepair material);
        bool Add(MaterialRepair material);
        bool Update(MaterialRepair material);
    }
}
