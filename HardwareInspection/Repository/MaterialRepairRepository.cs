﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using HardwareInspection.Model;

namespace HardwareInspection.Repository
{
    class MaterialRepairRepository : IMaterialRepairRepository
    {
        private readonly Context _context;

        public MaterialRepairRepository(Context context)
        {
            _context = context;
        }
        public List<MaterialRepair> MaterialRepairs
        {
            get
            {
                try
                {
                    List<MaterialRepair> materials = _context.MaterialRepairs.ToList();
                    return materials;
                }
                catch
                {
                    return null;
                }
            }
        }
        public bool Delete(MaterialRepair material)
        {
            try
            {
                _context.MaterialRepairs.Remove(material);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(MaterialRepair material)
        {
            try
            {
                _context.MaterialRepairs.Add(material);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(MaterialRepair material)
        {
            try
            {
                _context.MaterialRepairs.AddOrUpdate(material);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
