﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HardwareInspection.Repository;
using HardwareInspection.Model;
using Microsoft.Reporting.WinForms;


namespace HardwareInspection
{
    public partial class FrmHardRepairReport : Form
    {
        static readonly Context DbContext = new Context();
        private Equipment _tmpEquipment;
        readonly EquipmentRepository _equRepo = new EquipmentRepository(DbContext);
        readonly HardwareRepairRepository _hardRepairRepo = new HardwareRepairRepository(DbContext);
        private readonly DateTime _date1;
        private readonly DateTime _date2;
        ReportDataSource _rep1;

        public FrmHardRepairReport(DateTime date1, DateTime date2)
        {
            this._date1 = date1;
            this._date2 = date2;
            InitializeComponent();
        }

        private void FrmHardRepairReport_Load(object sender, EventArgs e)
        {
            DataTable table1 = GetTable(_date1, _date2);
            _rep1 = new ReportDataSource("DataSet1", table1);

            ReportParameter textbox1Param = new ReportParameter("ReportParameter1", _date1.ToShortDateString());
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            ReportParameter textbox2Param = new ReportParameter("ReportParameter3", _date2.ToShortDateString());
            reportViewer1.LocalReport.SetParameters(textbox2Param);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(_rep1);
            reportViewer1.RefreshReport();
        }

        public DataTable GetTable(DateTime date1, DateTime date2)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Номер", typeof(string));
            table.Columns.Add("Наименование", typeof(string));
            table.Columns.Add("Инв_номер", typeof(string));
            table.Columns.Add("Дата_ремонта", typeof(string));
            int i = 0;
            
            foreach (var list in _hardRepairRepo.HardwareRepairs.Where(n => n.DateReplaced >= date1 && n.DateReplaced <= date2))
            {
                _tmpEquipment = _equRepo.Equipments.Find(n => n.Id == list.EquipmentId);
                table.Rows.Add(++i, _tmpEquipment.NameEquipment, _tmpEquipment.InventoryNumber, list.DateReplaced.ToShortDateString());
            }
            return table;
        }

    }
}
