﻿using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using HardwareInspection.Model;
using HardwareInspection.Repository;

namespace HardwareInspection
{
    public partial class FrmReportRepair : Form
    {
        static readonly Context DbContext = new Context();
        readonly HardwareRepairRepository _hardRepo = new HardwareRepairRepository(DbContext);
        readonly MaterialRepairRepository _matRepairdRepo = new MaterialRepairRepository(DbContext);
        readonly PartRepairRepository _partRepairdRepo = new PartRepairRepository(DbContext);

        
        private readonly HardwareRepair _hardwareRepair;
        private readonly Employee _emp;
        private readonly Equipment _equ;
        private DateTime _date;
        public FrmReportRepair(HardwareRepair hardwareRepair, Equipment equ, Employee emp, DateTime date)
        {
            InitializeComponent();
            _hardwareRepair = hardwareRepair;
            _equ = equ;
            _emp = emp;
            _date = date;
        }

        private void frmView_Load(object sender, EventArgs e)
        {
            ReportDataSource _rep1;
            ReportDataSource _rep2;
            ReportParameter textbox1Param = new ReportParameter("ReportParameter1", _equ.NameEquipment);
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            ReportParameter textbox2Param = new ReportParameter("ReportParameter2", _equ.InventoryNumber);
            reportViewer1.LocalReport.SetParameters(textbox2Param);
            ReportParameter textbox3Param = new ReportParameter("ReportParameter3", _emp.LastName + " " + _emp.FirstName +" " +_emp.Patronymic);
            reportViewer1.LocalReport.SetParameters(textbox3Param);
            ReportParameter textbox4Param = new ReportParameter("ReportParameter4", _emp.EmployeeNumber);
            reportViewer1.LocalReport.SetParameters(textbox4Param);
            ReportParameter textbox5Param = new ReportParameter("ReportParameter5", _date.ToShortDateString());
            reportViewer1.LocalReport.SetParameters(textbox5Param);

            DataTable table1 = GetTable(1);
            _rep1 = new ReportDataSource("DataSet1", table1);

            DataTable table2 = GetTable(2);
            _rep2 = new ReportDataSource("DataSet2", table2);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(_rep1);
            reportViewer1.LocalReport.DataSources.Add(_rep2);
            reportViewer1.RefreshReport();
        }

        public DataTable GetTable(int i)
        {
            if (i == 2)
            {
                int k = 0;
                DataTable table = new DataTable();
                table.Columns.Add("Номер", typeof(string));
                table.Columns.Add("Наименование", typeof(string));
                table.Columns.Add("Единицы", typeof(string));
                table.Columns.Add("Количество", typeof(string));
                foreach (var list in _matRepairdRepo.MaterialRepairs.Where(t => t.HardwareRepairId == _hardwareRepair.Id))
                {
                    table.Rows.Add(++k, list.NameMaterial, list.UnitOfMeasure, list.Quantity.ToString(CultureInfo.InvariantCulture));
                }
                return table;
            }
            if (i == 1)
            {
                int k = 0;
                DataTable table = new DataTable();
                table.Columns.Add("Номер", typeof(string));
                table.Columns.Add("Наименование", typeof(string));
                table.Columns.Add("Единицы", typeof(string));
                table.Columns.Add("Количество", typeof(string));
                foreach (var list in _partRepairdRepo.PartRepairs.Where(t => t.HardwareRepairId == _hardwareRepair.Id))
                {
                    table.Rows.Add(++k, list.NamePart + " " + list.PartNumber, list.UnitOfMeasure, list.Quantity.ToString(CultureInfo.InvariantCulture));
                }
                return table;
            }
            return null;
        }
    }
}
