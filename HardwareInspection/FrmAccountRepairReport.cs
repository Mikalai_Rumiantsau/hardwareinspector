﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using HardwareInspection.Model;
using HardwareInspection.Repository;
using Microsoft.Reporting.WinForms;

namespace HardwareInspection
{
    public partial class FrmAccountRepairReport : Form
    {
        static readonly Context DbContext = new Context();
        private Organization _tmpOrganization;
        readonly AccountingRepairRepository _accountingRepairRepo = new AccountingRepairRepository(DbContext);
        readonly OrganizationRepository _orgRepairdRepo = new OrganizationRepository(DbContext);
        private readonly DateTime _date1;
        private readonly DateTime _date2;
        ReportDataSource _rep1;

        public FrmAccountRepairReport(DateTime date1, DateTime date2)
        {
            this._date1 = date1;
            this._date2 = date2;
            InitializeComponent();
        }

        private void FrmAccountReport_Load(object sender, EventArgs e)
        {
            DataTable table1 = GetTable(_date1, _date2);
            _rep1 = new ReportDataSource("DataSet1", table1);

            ReportParameter textbox1Param = new ReportParameter("ReportParameter1", _date1.ToShortDateString());
            reportViewer1.LocalReport.SetParameters(textbox1Param);
            ReportParameter textbox2Param = new ReportParameter("ReportParameter3", _date2.ToShortDateString());
            reportViewer1.LocalReport.SetParameters(textbox2Param);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(_rep1);
            reportViewer1.RefreshReport();
        }

        public DataTable GetTable(DateTime date1, DateTime date2)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Номер", typeof(string));
            table.Columns.Add("Наименование", typeof(string));
            table.Columns.Add("Дата", typeof(string));
            table.Columns.Add("Что_делали", typeof(string));
            table.Columns.Add("Стоимость", typeof(double));
            int i = 0;

            foreach (var list in _accountingRepairRepo.AccountingRepairs.Where(n => n.DateReplaced >= date1 && n.DateReplaced <= date2))
            {
                _tmpOrganization = _orgRepairdRepo.Organizations.Find(n => n.Id == list.OrganizationId);
                table.Rows.Add(++i, _tmpOrganization.NameOrganization, list.DateReplaced.ToShortDateString(), list.WhatWork, list.Сost);
            }
            return table;
        }
    }
}
