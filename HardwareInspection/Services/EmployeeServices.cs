﻿using System.Windows.Forms;
using HardwareInspection.Model;

namespace HardwareInspection.Services
{
    public class EmployeeServices
    {
        public void ClearEmployeesGrid(ListView listView)
        {
            listView.Columns.Clear();
            listView.Columns.Add("№ ", 30, HorizontalAlignment.Left);
            listView.Columns.Add("Имя", 120, HorizontalAlignment.Left);
            listView.Columns.Add("Фамилия", 120, HorizontalAlignment.Left);
            listView.Columns.Add("Отчество", 120, HorizontalAlignment.Left);
            listView.Columns.Add("Таб. номер", 85, HorizontalAlignment.Left);
            listView.Items.Clear();
        }

        public void AddToGrid(ListView listView, Employee employee)
        {
            ListViewItem parent;
            parent = listView.Items.Add(employee.Id.ToString());
            parent.SubItems.Add(employee.FirstName);
            parent.SubItems.Add(employee.LastName);
            parent.SubItems.Add(employee.Patronymic);
            parent.SubItems.Add(employee.EmployeeNumber);
        }

    }
}
