﻿using System.Windows.Forms;
using HardwareInspection.Model;
using System;
using System.Globalization;

namespace HardwareInspection.Services
{
    public class MaterialServices
    {
        public void ClearMaterialsGrid(ListView listView, int where)
        {
            if (where == 0)
            {
                listView.Columns.Clear();
                listView.Columns.Add("№ ", 30, HorizontalAlignment.Left);
                listView.Columns.Add("Наименование", 175, HorizontalAlignment.Left);
                listView.Columns.Add("Ед.изм.", 50, HorizontalAlignment.Left);
                listView.Columns.Add("Кол-во", 75, HorizontalAlignment.Left);
            }
            else
            {
                listView.Columns.Clear();
                listView.Columns.Add("№ ", 30, HorizontalAlignment.Left);
                listView.Columns.Add("Наименование", 90, HorizontalAlignment.Left);
                listView.Columns.Add("Ед.изм.", 25, HorizontalAlignment.Left);
                listView.Columns.Add("Кол-во", 35, HorizontalAlignment.Left);
            }
            
            listView.Items.Clear();
        }

        public void AddToGrid(ListView listView, Material material)
        {
            ListViewItem parent;
            parent = listView.Items.Add(material.Id.ToString());
            parent.SubItems.Add(material.NameMaterial);
            parent.SubItems.Add(material.UnitOfMeasure);
            parent.SubItems.Add(material.Quantity.ToString(CultureInfo.InvariantCulture));
        }

        public void AddToGridRepairView(ListView listView, MaterialRepair material)
        {
            ListViewItem parent;
            parent = listView.Items.Add(material.Id.ToString());
            parent.SubItems.Add(material.NameMaterial);
            parent.SubItems.Add(material.UnitOfMeasure);
            parent.SubItems.Add(material.Quantity.ToString(CultureInfo.InvariantCulture));
        }
    }
}
