﻿using System.Windows.Forms;
using HardwareInspection.Model;
using System;
using System.Globalization;

namespace HardwareInspection.Services
{
    public class HardwareRepairServices
    {
        public void ClearHardwareRepairsGrid(ListView listView)
        {
            listView.Columns.Clear();
            listView.Columns.Add("№ ", 35, HorizontalAlignment.Left);
            listView.Columns.Add("Наименование оборуд. и инв. номер", 200, HorizontalAlignment.Left);
            listView.Columns.Add("Дата", 100, HorizontalAlignment.Left);
            listView.Items.Clear();
        }

        public void AddToGrid(ListView listView, HardwareRepair hardwareRepair, Equipment equipment)
        {
            ListViewItem parent;
            parent = listView.Items.Add(hardwareRepair.Id.ToString());
            parent.SubItems.Add(equipment.NameEquipment + " " + equipment.InventoryNumber);
            parent.SubItems.Add(hardwareRepair.DateReplaced.ToShortDateString());
        }

    }
}
