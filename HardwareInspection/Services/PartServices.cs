﻿using System.Windows.Forms;
using HardwareInspection.Model;
using System.Globalization;

namespace HardwareInspection.Services
{
    public class PartServices
    {
        public void ClearPartsGrid(ListView listView, int where)
        {
            if (where == 0)
            {
                listView.Columns.Clear();
                listView.Columns.Add("№ ", 30, HorizontalAlignment.Left);
                listView.Columns.Add("Наименование", 200, HorizontalAlignment.Left);
                listView.Columns.Add("Парт номер", 100, HorizontalAlignment.Left);
                listView.Columns.Add("Ед.изм.", 50, HorizontalAlignment.Left);
                listView.Columns.Add("Кол-во", 75, HorizontalAlignment.Left);
            }
            else
            {
                listView.Columns.Clear();
                listView.Columns.Add("№ ", 30, HorizontalAlignment.Left);
                listView.Columns.Add("Наименование", 100, HorizontalAlignment.Left);
                listView.Columns.Add("Парт номер", 80, HorizontalAlignment.Left);
                listView.Columns.Add("Ед.изм.", 50, HorizontalAlignment.Left);
                listView.Columns.Add("Кол-во", 40, HorizontalAlignment.Left);
            }
            listView.Items.Clear();
        }

        public void AddToGrid(ListView listView, Part part)
        {
            ListViewItem parent;
            parent = listView.Items.Add(part.Id.ToString());
            parent.SubItems.Add(part.NamePart);
            parent.SubItems.Add(part.PartNumber);
            parent.SubItems.Add(part.UnitOfMeasure);
            parent.SubItems.Add(part.Quantity.ToString(CultureInfo.InvariantCulture));
        }
        
        public void AddToGridRepairView(ListView listView, PartRepair part)
        {
            ListViewItem parent;
            parent = listView.Items.Add(part.Id.ToString());
            parent.SubItems.Add(part.NamePart);
            parent.SubItems.Add(part.PartNumber);
            parent.SubItems.Add(part.UnitOfMeasure);
            parent.SubItems.Add(part.Quantity.ToString(CultureInfo.InvariantCulture));
        }

    }
}
