﻿using System.Linq;
using System.Windows.Forms;
using HardwareInspection.Model;

namespace HardwareInspection.Services
{
    public class AccountingRepairServices
    {
        public void ClearAccountingRepairGrid(ListView listView)
        {
            listView.Columns.Clear();
            listView.Columns.Add("№ ", 25, HorizontalAlignment.Left);
            listView.Columns.Add("Наименование организации", 150, HorizontalAlignment.Left);
            listView.Columns.Add("Дата", 90, HorizontalAlignment.Left);
            listView.Columns.Add("Что делали", 150, HorizontalAlignment.Left);
            listView.Columns.Add("Стоимость", 80, HorizontalAlignment.Left);
            listView.Items.Clear();
        }

        public void AddToGrid(ListView listView, AccountingRepair accountingRepair, Organization organization)
        {
            ListViewItem parent;
            parent = listView.Items.Add(accountingRepair.Id.ToString());
            parent.SubItems.Add(organization.NameOrganization);
            parent.SubItems.Add(accountingRepair.DateReplaced.ToShortDateString());
            parent.SubItems.Add(accountingRepair.WhatWork);
            parent.SubItems.Add(accountingRepair.Сost.ToString());
        }

    }
}
