﻿using System.Windows.Forms;
using HardwareInspection.Model;

namespace HardwareInspection.Services
{
    public class OrganizationServices
    {
        public void ClearOrganizationsGrid(ListView listView)
        {
            listView.Columns.Clear();
            listView.Columns.Add("№ ", 25, HorizontalAlignment.Left);
            listView.Columns.Add("Наименование", 200, HorizontalAlignment.Left);
            listView.Columns.Add("Адрес", 100, HorizontalAlignment.Left);
            listView.Columns.Add("Телефон", 50, HorizontalAlignment.Left);
            listView.Items.Clear();
        }

        public void AddToGrid(ListView listView, Organization organization)
        {
            ListViewItem parent;
            parent = listView.Items.Add(organization.Id.ToString());
            parent.SubItems.Add(organization.NameOrganization);
            parent.SubItems.Add(organization.Address);
            parent.SubItems.Add(organization.Phone);
        }

    }
}
