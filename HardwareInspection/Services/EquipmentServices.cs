﻿using System.Windows.Forms;
using HardwareInspection.Model;
using System;

namespace HardwareInspection.Services
{
    public class EquipmentServices
    {
        public void ClearEquipmentsGrid(ListView listView)
        {
            listView.Columns.Clear();
            listView.Columns.Add("№ ", 30, HorizontalAlignment.Left);
            listView.Columns.Add("Наименование", 200, HorizontalAlignment.Left);
            listView.Columns.Add("Инв.номер", 110, HorizontalAlignment.Left);
            listView.Columns.Add("Состояние", 120, HorizontalAlignment.Left);
            listView.Items.Clear();
        }

        public void AddToGrid(ListView listView, Equipment equipment)
        {
            ListViewItem parent;
            parent = listView.Items.Add(equipment.Id.ToString());
            parent.SubItems.Add(equipment.NameEquipment);
            parent.SubItems.Add(equipment.InventoryNumber);
            parent.SubItems.Add(Enum.GetName(typeof(Equipment.ConditionOfEquipment), equipment.Condition));
        }

    }
}
