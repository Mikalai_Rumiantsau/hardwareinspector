﻿namespace HardwareInspection
{
    partial class FrmWorkWithRepair
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdAccountingRepair = new System.Windows.Forms.ListView();
            this.grdHardwareRepair = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.lblIdAc = new System.Windows.Forms.Label();
            this.lblIdHard = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.grdMat = new System.Windows.Forms.ListView();
            this.label4 = new System.Windows.Forms.Label();
            this.grdParts = new System.Windows.Forms.ListView();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // grdAccountingRepair
            // 
            this.grdAccountingRepair.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdAccountingRepair.FullRowSelect = true;
            this.grdAccountingRepair.GridLines = true;
            this.grdAccountingRepair.Location = new System.Drawing.Point(3, 44);
            this.grdAccountingRepair.Name = "grdAccountingRepair";
            this.grdAccountingRepair.Size = new System.Drawing.Size(532, 262);
            this.grdAccountingRepair.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdAccountingRepair.TabIndex = 37;
            this.grdAccountingRepair.UseCompatibleStateImageBehavior = false;
            this.grdAccountingRepair.View = System.Windows.Forms.View.Details;
            this.grdAccountingRepair.SelectedIndexChanged += new System.EventHandler(this.grdAccountingRepair_SelectedIndexChanged);
            // 
            // grdHardwareRepair
            // 
            this.grdHardwareRepair.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdHardwareRepair.FullRowSelect = true;
            this.grdHardwareRepair.GridLines = true;
            this.grdHardwareRepair.Location = new System.Drawing.Point(541, 44);
            this.grdHardwareRepair.Name = "grdHardwareRepair";
            this.grdHardwareRepair.Size = new System.Drawing.Size(473, 262);
            this.grdHardwareRepair.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdHardwareRepair.TabIndex = 38;
            this.grdHardwareRepair.UseCompatibleStateImageBehavior = false;
            this.grdHardwareRepair.View = System.Windows.Forms.View.Details;
            this.grdHardwareRepair.SelectedIndexChanged += new System.EventHandler(this.grdHardwareRepair_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Фильтр даты с";
            // 
            // lblIdAc
            // 
            this.lblIdAc.AutoSize = true;
            this.lblIdAc.Location = new System.Drawing.Point(12, 28);
            this.lblIdAc.Name = "lblIdAc";
            this.lblIdAc.Size = new System.Drawing.Size(35, 13);
            this.lblIdAc.TabIndex = 43;
            this.lblIdAc.Text = "label3";
            // 
            // lblIdHard
            // 
            this.lblIdHard.AutoSize = true;
            this.lblIdHard.Location = new System.Drawing.Point(687, 29);
            this.lblIdHard.Name = "lblIdHard";
            this.lblIdHard.Size = new System.Drawing.Size(35, 13);
            this.lblIdHard.TabIndex = 44;
            this.lblIdHard.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 309);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 45;
            this.label3.Text = "Было в ремонте:";
            // 
            // listView1
            // 
            this.listView1.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(3, 325);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(532, 84);
            this.listView1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listView1.TabIndex = 46;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // grdMat
            // 
            this.grdMat.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdMat.FullRowSelect = true;
            this.grdMat.GridLines = true;
            this.grdMat.Location = new System.Drawing.Point(541, 325);
            this.grdMat.Name = "grdMat";
            this.grdMat.Size = new System.Drawing.Size(235, 84);
            this.grdMat.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdMat.TabIndex = 48;
            this.grdMat.UseCompatibleStateImageBehavior = false;
            this.grdMat.View = System.Windows.Forms.View.Details;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(541, 309);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(303, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "Было израсходовано материалы               запасные части:";
            // 
            // grdParts
            // 
            this.grdParts.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.grdParts.FullRowSelect = true;
            this.grdParts.GridLines = true;
            this.grdParts.Location = new System.Drawing.Point(782, 325);
            this.grdParts.Name = "grdParts";
            this.grdParts.Size = new System.Drawing.Size(232, 84);
            this.grdParts.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdParts.TabIndex = 49;
            this.grdParts.UseCompatibleStateImageBehavior = false;
            this.grdParts.View = System.Windows.Forms.View.Details;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(87, 7);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(91, 20);
            this.dateTimePicker1.TabIndex = 50;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(212, 7);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(91, 20);
            this.dateTimePicker2.TabIndex = 52;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "по";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(747, 7);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(91, 20);
            this.dateTimePicker3.TabIndex = 56;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(722, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "по";
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(629, 7);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(91, 20);
            this.dateTimePicker4.TabIndex = 54;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(546, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 53;
            this.label6.Text = "Фильтр даты с";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(420, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 57;
            this.button1.Text = "Печать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(892, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 58;
            this.button2.Text = "Печать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(318, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(38, 23);
            this.button3.TabIndex = 59;
            this.button3.Text = "OK";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(848, 7);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(38, 23);
            this.button4.TabIndex = 60;
            this.button4.Text = "OK";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(973, 8);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(44, 23);
            this.button5.TabIndex = 61;
            this.button5.Text = "АКТ";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // FrmWorkWithRepair
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 421);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimePicker3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.grdParts);
            this.Controls.Add(this.grdMat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblIdHard);
            this.Controls.Add(this.lblIdAc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grdHardwareRepair);
            this.Controls.Add(this.grdAccountingRepair);
            this.Name = "FrmWorkWithRepair";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Работа с ремонтами";
            this.Load += new System.EventHandler(this.FrmWorkWithRepair_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView grdAccountingRepair;
        private System.Windows.Forms.ListView grdHardwareRepair;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblIdAc;
        private System.Windows.Forms.Label lblIdHard;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView grdMat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView grdParts;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}