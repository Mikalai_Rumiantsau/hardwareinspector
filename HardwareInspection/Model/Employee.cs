﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HardwareInspection.Model
{
    public class Employee
    {
        public int Id { get; set; }
        [MinLength(1), MaxLength(100)]
        public string FirstName { get; set; }
        [MinLength(1), MaxLength(100)]
        public string LastName { get; set; }
        [MinLength(1), MaxLength(100)]
        public string Patronymic { get; set; }
        [MinLength(2), MaxLength(100)]
        public string EmployeeNumber { get; set; }
        public virtual ICollection<Equipment> Equipments { get; set; }
        public virtual ICollection<Material> Materials { get; set; }
        public virtual ICollection<Part> Parts { get; set; }
        public virtual ICollection<HardwareRepair> HardwareRepairs { get; set; }
    }
}
