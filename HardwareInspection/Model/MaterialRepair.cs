﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareInspection.Model
{
    public class MaterialRepair
    {
        public int HardwareRepairId { get; set; }
        public HardwareRepair HardwareRepair { get; set; }
        public int Id { get; set; }
        public string NameMaterial { get; set; }
        public string UnitOfMeasure { get; set; }
        public double Quantity { get; set; }
    }
}
