﻿using System;

namespace HardwareInspection.Model
{
    public class AccountingRepair
    {
        public int EquipmentId { get; set; }
        public Equipment Equipment { get; set; }
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }
        public int Id { get; set; }
        public DateTime DateReplaced { get; set; }
        public string WhatWork { get; set; }
        public double Сost { get; set; }
    }
}
