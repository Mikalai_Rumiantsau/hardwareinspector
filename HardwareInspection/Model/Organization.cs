﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareInspection.Model
{
    public class Organization
    {
        public int Id { get; set; }
        [MinLength(1), MaxLength(100)]
        public string NameOrganization { get; set; }
        [MinLength(1), MaxLength(200)]
        public string Address { get; set; }
        [MinLength(1), MaxLength(30)]
        public string Phone { get; set; }
        public virtual ICollection<AccountingRepair> AccountingRepairs { get; set; }
    }
}
