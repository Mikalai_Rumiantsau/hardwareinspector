﻿using System;

namespace HardwareInspection.Model
{
    public class Material
    {
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int Id { get; set; }
        public string NameMaterial { get; set; }
        public string UnitOfMeasure { get; set; }
        public double Quantity { get; set; }
    }
}
