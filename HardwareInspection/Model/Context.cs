﻿using System.Data.Entity;

namespace HardwareInspection.Model
{
    class Context : DbContext
    {
        public Context() : base("AppDir")
        { }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<AccountingRepair> AccountingRepairs { get; set; }
        public DbSet<HardwareRepair> HardwareRepairs { get; set; }
        public DbSet<MaterialRepair> MaterialRepairs { get; set; }
        public DbSet<PartRepair> PartRepairs { get; set; }
        public DbSet<Organization> Organizations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Equipment>().HasRequired(p => p.Employee);
            modelBuilder.Entity<Material>().HasRequired(p => p.Employee);
            modelBuilder.Entity<Part>().HasRequired(p => p.Employee);
            modelBuilder.Entity<AccountingRepair>().HasRequired(p => p.Equipment);
            modelBuilder.Entity<AccountingRepair>().HasRequired(p => p.Organization);
            modelBuilder.Entity<HardwareRepair>().HasRequired(p => p.Equipment);
            modelBuilder.Entity<HardwareRepair>().HasRequired(p => p.Employee);
            modelBuilder.Entity<MaterialRepair>().HasRequired(p => p.HardwareRepair);
            modelBuilder.Entity<PartRepair>().HasRequired(p => p.HardwareRepair);
        }
    }
}
