﻿using System;
using System.Collections.Generic;

namespace HardwareInspection.Model
{
    public class HardwareRepair
    {
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int EquipmentId { get; set; }
        public Equipment Equipment { get; set; }
        public int Id { get; set; }
        public DateTime DateReplaced { get; set; }
        public string Сomment { get; set; }
        public virtual ICollection<MaterialRepair> MaterialRepairs { get; set; }
        public virtual ICollection<PartRepair> PartRepairs { get; set; }
    }
}
