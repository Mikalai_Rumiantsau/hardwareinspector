﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardwareInspection.Model
{
    public class PartRepair
    {
        public int HardwareRepairId { get; set; }
        public HardwareRepair HardwareRepair { get; set; }
        public int Id { get; set; }
        public string NamePart { get; set; }
        public string PartNumber { get; set; }
        public string UnitOfMeasure { get; set; }
        public double Quantity { get; set; }
    }
}
