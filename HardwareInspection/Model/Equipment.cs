﻿using System.ComponentModel.DataAnnotations;

namespace HardwareInspection.Model
{
    public class Equipment
    {
        public enum ConditionOfEquipment
        {
            Good = 1,
            Bad = 2
        }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public int Id { get; set; }
        [MinLength(1), MaxLength(100)]
        public string NameEquipment { get; set; }
     //   [Range(0, int.MaxValue, ErrorMessage = "Please enter a value bigger than {0}")]
        [MinLength(3), MaxLength(20)]
        public string InventoryNumber { get; set; }
        public ConditionOfEquipment Condition { get; set; }
    }
}
